<?php

use App\Http\Controllers\Api\User\AdController;
use App\Http\Controllers\Api\User\ProductController;
use App\Http\Controllers\Api\User\CouponController;
use App\Http\Controllers\Api\Admin\TableController;
use App\Http\Controllers\Api\User\ContactController;
use App\Http\Controllers\Api\User\CouponUserController;
use App\Http\Controllers\Api\User\OrderController;
use App\Http\Controllers\Api\User\RestaurantController;
use App\Http\Controllers\Api\User\UserController;
use App\Http\Controllers\Api\User\RestaurantUserController;
use App\Http\Resources\User\ProductResource;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/


Route::group([
    "prefix" => 'product',

], function () {
    Route::get('/restaurant/{restaurant}', [ProductController::class, 'index']);
    Route::get('/{product}', [ProductController::class, 'show']);
});

Route::get('/', [UserController::class, 'show']);
Route::post('update', [UserController::class, 'update']);
Route::get('/order', [UserController::class, 'getOrders']);


Route::group([
    "prefix" => 'restaurant',
], function () {
    Route::get('/', [RestaurantController::class, 'index']);
    Route::post('/{restaurant}/toggleSubscribe', [RestaurantController::class, 'toggleSubscribe']);
    Route::get('/{restaurant}/show', [RestaurantController::class, 'show']);
});


Route::group([
    "prefix" => 'ad',
], function () {
    Route::get('/', [AdController::class, 'index']);
    Route::get('/{ad}', [AdController::class, 'show']);
});

Route::group([
    "prefix" => 'coupon',

], function () {
    Route::get('/', [CouponController::class, 'index']);
    Route::get('/user', [CouponController::class, 'userCoupons']);
    Route::get('/{coupon}', [CouponController::class, 'show']);
    Route::post('/{coupon}/buy', [CouponController::class, 'buyCoupon']);
});

Route::group([
    "prefix" => 'order',

], function () {
    Route::post('/restaurant/{restaurant}', [OrderController::class, 'store']);
    Route::get('/completed', [OrderController::class, 'index']);
    Route::get('/{order}/cart', [OrderController::class, 'showCart']);
    Route::post('/{order}', [OrderController::class, 'cancel']);
});


Route::group([
    "prefix" => 'contact',

], function () {
    Route::get('/', [ContactController::class, 'index']);
});
