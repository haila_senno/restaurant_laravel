<?php

use App\Http\Controllers\Api\Admin\AuthController as AdminAuthController;
use App\Http\Controllers\Api\User\AuthController as UserAuthController;
use App\Http\Controllers\Api\Waiter\AuthController as WaiterAuthController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthController;

Route::group([
    "prefix" => 'admin',
], function () {
    Route::post('login', [AdminAuthController::class, 'login']);
    Route::post('registerAdmin', [AdminAuthController::class, 'registerAdmin']);
});


Route::group([
    "prefix" => 'admin',
    "middleware" => 'auth:api_admin',
], function () {
    Route::post('logout', [AdminAuthController::class, 'logout']);
});


Route::group([
    "prefix" => 'user',
], function () {
    Route::post('login', [UserAuthController::class, 'login']);
    Route::post('register', [UserAuthController::class, 'register']);
});


Route::group([
    "prefix" => 'user',
    "middleware" => 'auth:api_user',
], function () {
    Route::post('logout', [UserAuthController::class, 'logout']);
});


Route::group([
    "prefix" => 'waiter',
], function () {
    Route::post('login', [WaiterAuthController::class, 'login']);
    Route::post('register', [WaiterAuthController::class, 'register']);
});


Route::group([
    "prefix" => 'waiter',
    "middleware" => 'auth:api_waiter',
], function () {
    Route::post('logout', [WaiterAuthController::class, 'logout']);
});
