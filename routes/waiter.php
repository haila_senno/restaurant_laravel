<?php

use App\Http\Controllers\Api\User\ProductController;
use App\Http\Controllers\Api\Admin\TableController;
use App\Http\Controllers\Api\Waiter\CartController;
use App\Http\Controllers\Api\Waiter\OrderController;
use App\Http\Controllers\Api\Waiter\RestaurantUserController;
use App\Http\Controllers\Api\Waiter\WaiterController;
use App\Models\RestaurantUser;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/



Route::group(
    [
        "prefix" => 'order',
    ],
    function () {
        Route::get('/pending',[OrderController::class,'indexPending']);
        Route::get('/',[OrderController::class,'index']);
        Route::get('/{order}/show',[OrderController::class,'show']);
        Route::post('/{order}/accept',[OrderController::class,'accept']);
        Route::post('/{order}/complete',[OrderController::class,'completeOrder']);
    }
);

Route::get('/show',[WaiterController::class,'show']);
Route::post('/update',[WaiterController::class,'update']);
