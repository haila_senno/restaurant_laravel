<?php

use App\Enums\AdminRoleEnum;
use App\Http\Controllers\Api\Admin\AdController;
use App\Http\Controllers\Api\Admin\RestaurantController;
use App\Http\Controllers\Api\Admin\AdminController;
use App\Http\Controllers\Api\Admin\CategoryController;
use App\Http\Controllers\Api\Admin\ContactController;
use App\Http\Controllers\Api\Admin\OwnerController;
use App\Http\Controllers\Api\Admin\ProductController;
use App\Http\Controllers\Api\Admin\CouponController;
use App\Http\Controllers\Api\Admin\OrderController;
use App\Http\Controllers\Api\Admin\SettingController;
use App\Http\Controllers\Api\Admin\TableController;
use App\Http\Controllers\Api\Admin\UserController;
use App\Http\Controllers\Api\Admin\WaiterController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::middleware(['role:' . AdminRoleEnum::ADMIN->value . '|' . AdminRoleEnum::OWNER->value])->group(function () {

    Route::group([
        "prefix" => 'product',

    ], function () {
        Route::get('/restaurant/{restaurant}/category/{category}', [ProductController::class, 'index']);
        Route::post('/restaurant/{restaurant}/category/{category}', [ProductController::class, 'store']);
        Route::get('/{product}', [ProductController::class, 'show']);
        Route::post('/{product}/update', [ProductController::class, 'update']);
        Route::delete('/{product}', [ProductController::class, 'destroy']);
    });

    Route::group([
        "prefix" => 'table'
    ], function () {
        Route::get('/restaurant/{restaurant}', [TableController::class, 'index']);
        Route::post('/restaurant/{restaurant}', [TableController::class, 'store']);
        Route::get('/{table}', [TableController::class, 'show']);
        Route::post('/{table}/update', [TableController::class, 'update']);
        Route::delete('/{table}', [TableController::class, 'destroy']);
    });

    Route::group(
        [
            "prefix" => 'category',
        ],
        function () {
            Route::get('/restaurant/{restaurant}', [CategoryController::class, 'index']);
            Route::post('/restaurant/{restaurant}', [CategoryController::class, 'store']);
            Route::get('/{category}', [CategoryController::class, 'show']);
            Route::post('/{category}/update', [CategoryController::class, 'update']);
            Route::delete('/{category}', [CategoryController::class, 'destroy']);
        }
    );

    Route::group([
        "prefix" => 'waiter',

    ], function () {
        Route::get('/', [WaiterController::class, 'index']);
        Route::post('/', [WaiterController::class, 'store']);
        Route::get('/{waiter}', [WaiterController::class, 'show']);
        Route::post('/{waiter}/update', [WaiterController::class, 'update']);
        Route::delete('/{waiter}', [WaiterController::class, 'destroy']);
    });

    Route::group([
        "prefix" => 'restaurant',
        "as" =>'restaurant.'
    ], function () {
        Route::get('/', [RestaurantController::class, 'index'])->name('index');
        Route::get('/{restaurant}', [RestaurantController::class, 'show'])->name('show');
        Route::post('/{restaurant}/update', [RestaurantController::class, 'update'])->name('update');
    });

    Route::group([
        "prefix" => 'ad',

    ], function () {
        Route::post('/', [AdController::class, 'store']);
        Route::get('/', [AdController::class, 'index']);
        Route::get('/{ad}', [AdController::class, 'show']);
        Route::post('/{ad}/update', [AdController::class, 'update']);
        Route::delete('/{ad}', [AdController::class, 'destroy']);
        Route::post('/{ad}/toggle', [AdController::class, 'toggleActive']);
    });
});

Route::middleware(['role:' . AdminRoleEnum::ADMIN->value])->group(function () {

    Route::group([
        "prefix" => 'restaurant',
        "as" => 'restaurant.'
    ], function () {
        Route::post('/', [RestaurantController::class, 'store'])->name('store');
        Route::delete('/{restaurant}', [RestaurantController::class, 'destroy'])->name('destroy');
    });



    Route::group([
        "prefix" => 'admin',

    ], function () {
        Route::get('/', [AdminController::class, 'show']);
        Route::post('/update', [AdminController::class, 'update']);
    });



    Route::group([
        "prefix" => 'coupon',

    ], function () {
        Route::post('/', [CouponController::class, 'store']);
        Route::get('/', [CouponController::class, 'index']);
        Route::get('/user', [CouponController::class, 'mostUsed']);
        Route::get('/{coupon}', [CouponController::class, 'show']);
        Route::post('/{coupon}/update', [CouponController::class, 'update']);
        Route::delete('/{coupon}', [CouponController::class, 'destroy']);
    });

    Route::group([
        "prefix" => 'setting',

    ], function () {
        Route::get('/', [SettingController::class, 'index']);
        Route::post('/', [SettingController::class, 'store']);
        Route::post('/{setting}/update', [SettingController::class, 'update']);
        Route::delete('/{setting}', [SettingController::class, 'destroy']);
    });

    Route::group([
        "prefix" => 'user',
    ], function () {
        Route::get('/', [UserController::class, 'index']);
        Route::get('/coupon', [UserController::class, 'mostUsingCoupons']);
        Route::post('/', [UserController::class, 'store']);
        Route::get('/{user}', [UserController::class, 'show']);
        Route::post('/{user}/update', [UserController::class, 'update']);
        Route::delete('/{user}', [UserController::class, 'destroy']);
    });



    Route::group(
        [
            "prefix" => 'order',
        ],
        function () {
            Route::get('/', [OrderController::class, 'index']);
            Route::get('/user/{user}', [OrderController::class, 'indexUser']);
            // Route::post('/restaurant/{restaurant}', [OrderController::class, 'store']);
            Route::get('/{order}', [OrderController::class, 'show']);
            Route::post('/{order}/update', [OrderController::class, 'update']);
            // Route::post('/{category}', [OrderController::class, 'update']);
            // Route::delete('/{category}', [OrderController::class, 'destroy']);
        }
    );

    Route::group(
        [
            "prefix" => 'owner',
        ],
        function () {
            Route::get('/', [OwnerController::class, 'index']);
            Route::post('/', [OwnerController::class, 'store']);
            Route::post('/{owner}/update', [OwnerController::class, 'update']);
            Route::delete('/{owner}', [OwnerController::class, 'destroy']);
            Route::get('/{owner}', [OwnerController::class, 'show']);

            // Route::get('/user/{user}', [OrderController::class, 'indexUser']);
            // Route::post('/restaurant/{restaurant}', [OrderController::class, 'store']);
            // Route::get('/{order}', [OrderController::class, 'show']);
            // Route::post('/{category}', [OrderController::class, 'update']);
            // Route::delete('/{category}', [OrderController::class, 'destroy']);
        }
    );


    Route::group([
        "prefix" => 'contact',
    ], function () {
        Route::get('/', [ContactController::class, 'index']);
        Route::post('/', [ContactController::class, 'store']);
        Route::post('/{contact}/update', [ContactController::class, 'update']);
        Route::delete('/{contact}', [ContactController::class, 'destroy']);
        Route::post('/priority', [ContactController::class, 'updatePriorities']);
    });
});
