<?php

namespace App\Policies;

use App\Enums\AdminPermissionEnum;
use App\Models\Admin;
use App\Models\Restaurant;
use App\Models\User;
use App\Models\Waiter;
use Illuminate\Auth\Access\HandlesAuthorization;

class WaiterPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any models.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function viewAny(Admin $admin)
    {
        return ($admin->can(AdminPermissionEnum::SHOW_WAITER->value));
    }

    /**
     * Determine whether the user can view the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Waiter  $waiter
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function view(Admin $admin, Waiter $waiter)
    {

        $restaurant = Restaurant::find($waiter->restaurant_id);

        return ($admin->can(AdminPermissionEnum::DELETE_WAITER->value) &&
            $restaurant && $restaurant->owner_id === $admin->id
        );
    }

    /**
     * Determine whether the user can create models.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function create(User $user)
    {
        //
    }

    /**
     * Determine whether the user can update the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Waiter  $waiter
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function update(User $user, Waiter $waiter)
    {
        //
    }

    /**
     * Determine whether the user can delete the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Waiter  $waiter
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function delete(Admin $admin, Waiter $waiter)
    {
        $restaurant = Restaurant::find($waiter->restaurant_id);

        return ($admin->can(AdminPermissionEnum::DELETE_WAITER->value) &&
            $restaurant && $restaurant->owner_id === $admin->id
        );
    }

    /**
     * Determine whether the user can restore the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Waiter  $waiter
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function restore(User $user, Waiter $waiter)
    {
        //
    }

    /**
     * Determine whether the user can permanently delete the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Waiter  $waiter
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function forceDelete(User $user, Waiter $waiter)
    {
        //
    }
}
