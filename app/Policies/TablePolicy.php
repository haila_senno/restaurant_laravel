<?php

namespace App\Policies;

use App\Enums\AdminPermissionEnum;
use App\Models\Admin;
use App\Models\Restaurant;
use App\Models\Table;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class TablePolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any models.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function viewAny(Admin $admin,Restaurant $restaurant)
    {
        return ($admin->can(AdminPermissionEnum::SHOW_TABLE->value) &&
            $admin->id === $restaurant->owner_id);
    }

    /**
     * Determine whether the user can view the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Table  $table
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function view(Admin $admin, Table $table)
    {
        $restaurant = Restaurant::find($table->restaurant_id);

        return ($admin->can(AdminPermissionEnum::SHOW_TABLE->value) &&
            $restaurant && $restaurant->owner_id === $admin->id
        );
    }

    /**
     * Determine whether the user can create models.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function create(Admin $admin,$restaurant)
    {
        return ($admin->can(AdminPermissionEnum::CREATE_TABLE->value) &&
        $admin->id === $restaurant->owner_id);
}


    /**
     * Determine whether the user can update the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Table  $table
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function update(User $user, Table $table)
    {
        //
    }

    /**
     * Determine whether the user can delete the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Table  $table
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function delete(Admin $admin, Table $table)
    {
        $restaurant = Restaurant::find($table->restaurant_id);

        return ($admin->can(AdminPermissionEnum::DELETE_TABLE->value) &&
            $restaurant && $restaurant->owner_id === $admin->id
        );
    }

    /**
     * Determine whether the user can restore the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Table  $table
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function restore(User $user, Table $table)
    {
        //
    }

    /**
     * Determine whether the user can permanently delete the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Table  $table
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function forceDelete(User $user, Table $table)
    {
        //
    }
}
