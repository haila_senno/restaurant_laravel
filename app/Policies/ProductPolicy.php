<?php

namespace App\Policies;

use App\Enums\AdminPermissionEnum;
use App\Models\Admin;
use App\Models\Product;
use App\Models\Restaurant;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class ProductPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any models.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function viewAny(Admin $admin, Restaurant $restaurant)
    {
        return (
            $admin->can(AdminPermissionEnum::SHOW_PRODUCT->value) &&
            $admin->id === $restaurant->owner_id
        );
    }

    /**
     * Determine whether the user can view the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function view(Admin $admin, Product $product)
    {
        $restaurant = $product->categoryRestaurant->restaurant;
        return (
            $admin->can(AdminPermissionEnum::SHOW_PRODUCT->value) &&
             $restaurant->owner_id === $admin->id
        );
    }

    /**
     * Determine whether the user can create models.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function create(Admin $admin,Restaurant $restaurant)
    {
        return (
            $admin->can(AdminPermissionEnum::CREATE_PRODUCT->value) &&
            $admin->id === $restaurant->owner_id
        );
    }

    /**
     * Determine whether the user can update the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function update(User $user, Product $product)
    {
        //
    }

    /**
     * Determine whether the user can delete the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function delete(Admin $admin, Product $product)
    {
        $restaurant = $product->categoryRestaurant->restaurant;
        return (
            $admin->can(AdminPermissionEnum::DELETE_PRODUCT->value) &&
             $restaurant->owner_id === $admin->id
        );


    }

    /**
     * Determine whether the user can restore the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function restore(User $user, Product $product)
    {
        //
    }

    /**
     * Determine whether the user can permanently delete the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function forceDelete(User $user, Product $product)
    {
        //
    }
}
