<?php

namespace App\Policies;

use App\Enums\AdminPermissionEnum;
use App\Models\Admin;
use App\Models\Category;
use App\Models\Restaurant;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class CategoryPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any models.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function viewAny(Admin $admin, Restaurant $restaurant)
    {
        //  if(($admin instanceof Admin) && $admin->can(AdminPermissionEnum::SHOW_CATEGORY->value) && $restaurant->owner_id == $admin->id);
        return (
            $admin->can(AdminPermissionEnum::SHOW_CATEGORY->value) &&
            $restaurant->owner_id === $admin->id
        );
    }

    /**
     * Determine whether the user can view the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Category  $category
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function view(Admin $admin, Category $category)
    {

        $restaurantIds = $category->restaurants->pluck('owner_id')->toArray();
        return (
            $admin->can(AdminPermissionEnum::SHOW_CATEGORY->value) &&
            in_array($admin->id, $restaurantIds));
    }

    /**
     * Determine whether the user can create models.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function create(Admin $admin, Restaurant $restaurant)
    {
        return (
            $admin->can(AdminPermissionEnum::CREATE_CATEGORY->value) &&
            $admin->id === $restaurant->owner_id
        );
    }

    /**
     * Determine whether the user can update the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Category  $category
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function update(User $user, Category $category)
    {
        //
    }

    /**
     * Determine whether the user can delete the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Category  $category
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function delete(Admin $admin, Category $category)
    {

        $restaurantIds = $category->restaurants->pluck('owner_id')->toArray();
        return (
            $admin->can(AdminPermissionEnum::DELETE_CATEGORY->value) &&
            in_array($admin->id, $restaurantIds));
    }

    /**
     * Determine whether the user can restore the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Category  $category
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function restore(User $user, Category $category)
    {
        //
    }

    /**
     * Determine whether the user can permanently delete the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Category  $category
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function forceDelete(User $user, Category $category)
    {
        //
    }
}
