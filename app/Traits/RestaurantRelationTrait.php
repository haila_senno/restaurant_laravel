<?php
namespace App\Traits;

use App\Models\Restaurant;
use Illuminate\Database\Eloquent\Relations\BelongsTo;


trait RestaurantRelationTrait
{
    public function restaurant(): BelongsTo
    {
        return $this->belongsTo(Restaurant::class);
    }

}
