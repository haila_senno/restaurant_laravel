<?php

namespace App\Console\Commands;

use App\Models\CouponUser;
use Carbon\Carbon;
use Illuminate\Console\Command;

class DeleteExpiredCoupon extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'app:delete-expired-coupon';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'delete coupons for users that were created 7 days ago';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        CouponUser::where('created_at', '<', Carbon::now()->subDays(7))->delete();
    }
}
