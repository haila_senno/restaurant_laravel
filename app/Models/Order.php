<?php

namespace App\Models;

use App\Enums\OrderStatusEnum;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasManyThrough;
use Illuminate\Database\Eloquent\Relations\HasOneThrough;

//use Spatie\MediaLibrary\HasMedia;
//use Spatie\MediaLibrary\InteractsWithMedia;

class Order extends Model //implements HasMedia
{
    use HasFactory;
    //use InteractsWithMedia;

    protected $guarded = [];

    protected $casts = [];

    protected $attributes = [
        "status" => OrderStatusEnum::PENDING,
    ];





    ########## Relations ##########


    public function products(): BelongsToMany
    {
        return $this->belongsToMany(Product::class, 'carts')
        ->withPivot(['count','price','details'])
        ->withTimestamps();
    }

    public function user(): BelongsTo
    {
        return $this->BelongsTo(User::class);
    }

    public function couponUser(): BelongsTo
    {
        return $this->BelongsTo(CouponUser::class);
    }


    public function waiter(): BelongsTo
    {
        return $this->BelongsTo(Waiter::class);
    }

    public function table(): BelongsTo
    {
        return $this->BelongsTo(Table::class);
    }


      //######### Scopes ##########
      public function scopePending($query): void
      {
          $query->where('status',OrderStatusEnum::PENDING->value)->latest('id');
      }

      public function scopeCompleted($query): void
      {
          $query->where('status',OrderStatusEnum::COMPLETED->value)->latest('id');
      }

      public function scopeStatus($query,$status): void
      {
          $query->where('status',$status);
      }


    ########## Libraries ##########


    // public function registerMediaCollections(): void
    // {
    //     $this
    //         ->addMediaCollection('Order')
    //         ->useFallbackUrl(env('APP_URL') . '/images/default.jpg')
    //         ->singleFile();
    // }



    protected static function boot()
    {
        parent::boot();

        static::updating(function ($model) {
            if ($model->isDirty('status') && $model->status == OrderStatusEnum::PREPARING->value);
            $model->user->increment('total_points', $model->total_points);

        });
    }




}
