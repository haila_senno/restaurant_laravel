<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;

class Ad extends Model implements HasMedia
{
    use HasFactory;
    use InteractsWithMedia;

    protected $guarded = [];

    protected $casts = [];

    protected $with = ['media','restaurant.owner'];

 

    ########## Relations ##########

    public function restaurant(): BelongsTo
    {
        return $this->belongsTo(Restaurant::class);
    }

    ########## Libraries ##########


    public function registerMediaCollections(): void
    {
        $this
            ->addMediaCollection('Ad')
            ->useFallbackUrl(env('APP_URL') . '/images/default.jpg')
            ->singleFile();
    }

    public function scopeActive($query)
    {
        $now = now();
        $future = now()->addHours(24);
        return $query->whereBetween('added_at', [$now, $future]);
    }
}
