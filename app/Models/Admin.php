<?php

namespace App\Models;

use App\Enums\AdminRoleEnum;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Spatie\Permission\Traits\HasRoles;
use PHPOpenSourceSaver\JWTAuth\Contracts\JWTSubject;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;

class Admin extends Authenticatable implements JWTSubject, HasMedia
{
    use HasFactory;
    use HasRoles;
    use InteractsWithMedia;

    //use InteractsWithMedia;

    protected $guard = 'api_admin';
    protected $guarded = [];
    protected $with = ['roles'];


    protected $casts = [
        'password' => 'hashed',
    ];

    public function restaurants(): HasMany
    {
        return $this->hasMany(Restaurant::class,'owner_id');
    }

    public function scopeOwners($query)
    {
        return $query->role(AdminRoleEnum::OWNER->value);
    }

    ########## Relations ##########



    ########## Libraries ##########


    public function registerMediaCollections(): void
    {
        $this
            ->addMediaCollection('Admin')
            ->useFallbackUrl(env('APP_URL') . '/images/default.jpg')
            ->singleFile();
    }
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }
}
