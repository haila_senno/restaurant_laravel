<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasManyThrough;

//use Spatie\MediaLibrary\HasMedia;
//use Spatie\MediaLibrary\InteractsWithMedia;

class Coupon extends Model //implements HasMedia
{
    use HasFactory;
    //use InteractsWithMedia;

    protected $guarded = [];

    protected $casts = [];

    protected $attributes = [
        'is_available' => true
    ];




    ########## Relations ##########


    public function users(): BelongsToMany
    {
        return $this->belongsToMany(User::class, 'coupon_users')
            ->withPivot('id')
            ->withTimestamps();;
    }

    /**
     * Get all of the comments for the Coupon
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasManyThrough
     */
    public function orders(): HasManyThrough
    {
        return $this->hasManyThrough(Order::class, CouponUser::class);
    }

    public function scopeIsAvailable($query)
    {
        return $query->where('is_available', true);
    }

    public function scopeCouponDates($query, $startDate, $endDate)
    {
        return $query->whereHas('users', function ($q) use ($startDate, $endDate) {
            $q->whereDate('coupon_users.created_at', '>=', $startDate)
              ->whereDate('coupon_users.created_at', '<=', $endDate);
        });
    }

    ########## Libraries ##########


    // public function registerMediaCollections(): void
    // {
    //     $this
    //         ->addMediaCollection('Coupon')
    //         ->useFallbackUrl(env('APP_URL') . '/images/default.jpg')
    //         ->singleFile();
    // }

}
