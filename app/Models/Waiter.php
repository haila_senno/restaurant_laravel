<?php

namespace App\Models;

use App\Enums\OrderStatusEnum;
use App\Traits\RestaurantRelationTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Foundation\Auth\User as Authenticatable;
use PHPOpenSourceSaver\JWTAuth\Contracts\JWTSubject;

use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;

class Waiter extends Authenticatable implements JWTSubject , HasMedia
{
    use HasFactory, RestaurantRelationTrait,InteractsWithMedia;

    //use InteractsWithMedia;
    protected $guard = 'api_waiter';

    protected $guarded = [];

    protected $casts = [
        'password' => 'hashed',
    ];
    

    //######### Relations ##########

    /**
     * Get all of the orders for the Waiter
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function orders(): HasMany
    {
        return $this->hasMany(Order::class);
    }

    //######### Libraries ##########

    public function registerMediaCollections(): void
    {
        $this
            ->addMediaCollection('Waiter')
            ->useFallbackUrl(env('APP_URL') . '/images/default.jpg')
            ->singleFile();
    }

    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }
}
