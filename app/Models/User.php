<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use PHPOpenSourceSaver\JWTAuth\Contracts\JWTSubject;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;

class User extends Authenticatable implements JWTSubject ,HasMedia
{
    use HasFactory, Notifiable,InteractsWithMedia;

    protected $guard = 'api_user';

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $guarded = [];

    protected $with = ['media'];

    protected $attributes = [
        "total_points" => 0,
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
        'password' => 'hashed',
    ];

    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }

    public function restaurants(): BelongsToMany
    {
        return $this->belongsToMany(Restaurant::class, 'restaurant_users')
            ->withTimestamps();
    }

    public function coupons(): BelongsToMany
    {
        return $this->belongsToMany(Coupon::class, 'coupon_users')
            ->withPivot('id')
            ->withTimestamps();
    }


    public function notifications(): BelongsToMany
    {
        return $this->belongsToMany(Notification::class, 'notification_users')
            ->withPivot(['read_at'])
            ->withTimestamps();
    }

    public function orders(): HasMany
    {
        return $this->hasMany(Order::class);
    }

    public function checkPoints(Coupon $coupon)
    {
        return $this->total_points >= $coupon->points ? true : false;
    }

    public function scopeStartDate($query,$startDate)
    {
        return $query->whereHas('coupons', function ($q) use ($startDate) {
            $q->whereDate('coupon_users.created_at', ">=", $startDate);
         });
    }

    public function scopeEndDate($query,$endDate)
    {
        return $query->whereHas('coupons', function ($q) use ($endDate) {
            $q->whereDate('coupon_users.created_at', "<=", $endDate);
         });
    }

    public function scopeCouponDates($query, $startDate, $endDate)
    {
        return $query->whereHas('coupons', function ($q) use ($startDate, $endDate) {
            $q->whereDate('coupon_users.created_at', '>=', $startDate)
              ->whereDate('coupon_users.created_at', '<=', $endDate);
        });
    }

    //######### Libraries ##########

    public function registerMediaCollections(): void
    {
        $this
            ->addMediaCollection('User')
            ->useFallbackUrl(env('APP_URL') . '/images/default.jpg')
            ->singleFile();
    }

}
