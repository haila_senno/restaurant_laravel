<?php

namespace App\Models;

use App\Traits\RestaurantRelationTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

//use Spatie\MediaLibrary\HasMedia;
//use Spatie\MediaLibrary\InteractsWithMedia;

class Table extends Model //implements HasMedia
{
    use HasFactory, RestaurantRelationTrait;
    //use InteractsWithMedia;

    protected $guarded = [];

    protected $casts = [];

    public function orders(): HasMany
    {
        return $this->hasMany(Order::class);
    }

    //######### Relations ##########

    /**
     * Get the user that owns the Table
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */

    //######### Libraries ##########

    // public function registerMediaCollections(): void
    // {
    //     $this
    //         ->addMediaCollection('Table')
    //         ->useFallbackUrl(env('APP_URL') . '/images/default.jpg')
    //         ->singleFile();
    // }

}
