<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasManyThrough;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;

class Restaurant extends Model implements HasMedia
{
    use HasFactory, InteractsWithMedia;

    // public $translatedAttributes = ['name', 'description'];
    protected $guarded = [];

    protected $casts = [];

    //######### Relations ##########

    public function waiters(): HasMany
    {
        return $this->hasMany(Waiter::class);
    }

    public function tables(): HasMany
    {
        return $this->hasMany(Table::class);
    }



    public function owner(): BelongsTo
    {
        return $this->belongsTo(Admin::class, 'owner_id');
    }

    public function users(): BelongsToMany
    {
        return $this->belongsToMany(User::class, 'restaurant_users')
            ->withTimestamps();
    }

    public function categories(): BelongsToMany
    {
        return $this->belongsToMany(Category::class,'category_restaurants')
        ->using(CategoryRestaurant::class)
        ->withPivot(['id'])
        ->withTimestamps();
    }

    public function products(): HasManyThrough
    {
        return $this->hasManyThrough(Product::class, CategoryRestaurant::class,'restaurant_id','category_restaurant_id');
    }

    public function ads(): HasMany
    {
        return $this->HasMany(Ad::class);
    }

    //######### Libraries ##########

    public function registerMediaCollections(): void
    {
        $this
            ->addMediaCollection('Restaurant')
            ->useFallbackUrl(env('APP_URL').'/images/default.jpg')
            ->singleFile();
    }
}
