<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\Pivot;

//use Spatie\MediaLibrary\HasMedia;
//use Spatie\MediaLibrary\InteractsWithMedia;

class CategoryRestaurant extends Pivot //implements HasMedia
{
    use HasFactory;

    //use InteractsWithMedia;
    protected $table = 'category_restaurants';

    protected $guarded = [];

    protected $casts = [];

    //######### Relations ##########

    public function products(): HasMany
    {
        return $this->hasMany(Product::class,'category_restaurant_id');
    }


    public function restaurant(): BelongsTo
    {
        return $this->belongsTo(Restaurant::class);
    }

    public function category(): BelongsTo
    {
        return $this->belongsTo(Category::class);
    }

    //######### Libraries ##########

    // public function registerMediaCollections(): void
    // {
    //     $this
    //         ->addMediaCollection('CategoryRestaurant')
    //         ->useFallbackUrl(env('APP_URL') . '/images/default.jpg')
    //         ->singleFile();
    // }

}
