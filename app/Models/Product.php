<?php

namespace App\Models;

use App\Traits\RestaurantRelationTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;

class Product extends Model implements HasMedia
{
    use HasFactory, RestaurantRelationTrait;

    use InteractsWithMedia;

    protected $guarded = [];

    protected $casts = [];

    protected $with = ['media'];

    protected $attributes = ['is_shown' => true];



    public function orders(): BelongsToMany
    {
        return $this->belongsToMany(Order::class, 'carts')
            ->withPivot(['count', 'price','details'])
            ->withTimestamps();
    }

    public function categoryRestaurant(): BelongsTo
    {
        return $this->BelongsTo(CategoryRestaurant::class,'category_restaurant_id');
    }

    public function scopeIsShown($query)
    {
        return $query->where('is_Shown',true);
    }

    //######### Libraries ##########

    public function registerMediaCollections(): void
    {
        $this
            ->addMediaCollection('Product')
            ->useFallbackUrl(env('APP_URL') . '/images/default.jpg')
            ->singleFile();
    }
}
