<?php

namespace App\Actions;

use App\Enums\AdminRoleEnum;
use App\Models\Restaurant;
use Spatie\QueryBuilder\AllowedFilter;
use Spatie\QueryBuilder\QueryBuilder;

class GetRestaurantsAction
{
    public function execute($admin)
    {
        $query = QueryBuilder::for(Restaurant::class)
            ->with('owner')
            ->when($admin->hasRole(AdminRoleEnum::OWNER->value), function ($query) use ($admin) {
                $query->where('owner_id', $admin->id);
            })
            ->withCount(['products', 'categories'])
            ->allowedFilters([
                "name",
                AllowedFilter::exact("owner_name", "owner.name"),

            ])
            ->latest('id');
      
        return $query;
    }
}
