<?php

namespace App\Actions;

use App\Enums\AdminRoleEnum;
use App\Models\Restaurant;
use App\Models\Waiter;
use Spatie\QueryBuilder\AllowedFilter;
use Spatie\QueryBuilder\QueryBuilder;

class GetWaitersAction
{
    public function execute($admin)
    {
        $query = QueryBuilder::for(Waiter::class)
            ->with(['restaurant', 'media'])
            ->when($admin->hasRole(AdminRoleEnum::OWNER->value), function ($query) use ($admin) {
                $query->whereHas('restaurant', function ($q) use ($admin) {
                    $q->where('owner_id', $admin->id);
                });
            })
            ->withCount('orders')
            ->allowedFilters(
                'name',
                AllowedFilter::exact('restaurant_name', 'restaurant.name')
            )
            ->latest('id');

        return $query;
    }
}
