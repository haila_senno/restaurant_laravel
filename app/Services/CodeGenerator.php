<?php

namespace App\Services;

use Illuminate\Support\Str;

class CodeGenerator
{
    public static function generateUniqueCode($model, $attribute, $length)
    {
        $code = Str::random($length);

        while ($model::where($attribute, $code)->exists()) {
            $code = Str::random($length);
        }

        return $code;
    }
}
