<?php

namespace App\Services;

use App\Enums\OrderStatusEnum;
use App\Models\Coupon;
use App\Models\Order;
use App\Models\Product;
use App\Models\Setting;
use App\Models\Table;
use App\Models\User;
use Illuminate\Support\Facades\Auth;

class OrderService
{
    public function storeOrder($orderRequest, $cartRequest, $coupon, $user, $restaurant)
    {
        $products = $this->getProducts($cartRequest);

        $totals = $this->calculateTotals($products, $cartRequest, $restaurant);

        $result = $coupon ?
            $this->checkCouponStatus($orderRequest, $totals, $user, $products, $cartRequest, $coupon, $restaurant) :
            $this->createOrderWithoutCoupon($orderRequest, $totals, $user, $products, $cartRequest, $restaurant);


        return $result;
    }

    public function checkPendingOrders(User $user)
    {

        return $user->orders()->where('status', OrderStatusEnum::PENDING->value)->first();
    }

    public function checkOrdersTables(User $user, $tableId,$restaurant)
    {
        $table = $restaurant->tables()->where('id', $tableId)->first();

        return $table->orders()->where('user_id', $user->id)
            ->where('status', OrderStatusEnum::PREPARING->value)
            ->first();
    }


    public function getProducts($cartRequest)
    {
        $productsIds = array_column($cartRequest['order_products'], 'product_id');
        $productsIdsString = implode(',', $productsIds);
        $products = Product::whereIn('id', $productsIds)->orderByRaw("FIELD(id,$productsIdsString)")
            ->with('restaurant.waiters')
            ->get();

        return $products;
    }

    public function calculateTotals($products, $cartRequest, $restaurant)
    {
        $total_price = $restaurant->tax;
        $total_points = 0;
        // $total_period = 0;
        $price = 0;

        foreach ($products as $index => $product) {
            $total_price += $product->price * $cartRequest['order_products'][$index]['count'];

            // $total_period += $product->period;
            $price += $product->price * $cartRequest['order_products'][$index]['count'];
        }
        $total_points = Setting::getPoints($total_price);

        return [
            'total_price' => $total_price,
            'total_points' => $total_points,
            // 'total_period' => $total_period,
            'price' => $price,
        ];
    }

    public function checkCouponStatus($orderRequest, $totals, $user, $products, $cartRequest, $coupon_id, $restaurant)
    {
        $coupon = $user->coupons()->where('coupon_id', $coupon_id)->firstOrFail();
        if ($totals['total_price'] < $coupon->min_bill) {
            $result = [
                'order' => null,
                'message' => 'your bill is less than the bill you must have to use this coupon',
            ];
        } else {
            $waiter = $this->findWaiter($restaurant);
            $result = $this->createOrderWithCoupon($user, $coupon, $totals, $orderRequest, $waiter, $products, $cartRequest);
        }

        return $result;
    }

    public function createOrderWithCoupon($user, $coupon, $totals, $orderRequest, $waiter, $products, $cartRequest)
    {
        $coupon_user = $user->coupons()->where('coupon_id', $coupon->id)->first()->pivot;
        $total_points = Setting::getPoints($totals['total_price'] - $coupon->discount);
        $order = Order::Create(
            $orderRequest
                + [
                    'total_price' => $totals['total_price'] - $coupon->discount,
                    // 'total_period' => $totals['total_period'],
                    'price' => $totals['price'],
                    'total_points' => $total_points,
                    'coupon_user_id' => $coupon_user->id,
                    'waiter_id' => $waiter->id,
                    'user_id' => $user->id,
                ],
        );
        $this->createCart($products, $cartRequest, $order);
        $result = [
            'order' => $order,
            'message' => 'order is added success',
        ];
        $user->increment('total_points', $total_points);

        return $result;
    }

    public function createOrderWithoutCoupon($orderRequest, $totals, $user, $products, $cartRequest, $restaurant)
    {
        $waiter = $this->findWaiter($restaurant);

        $order = Order::Create(
            $orderRequest
                + [
                    'total_price' => $totals['total_price'],
                    'price' => $totals['price'],
                    // 'total_period' => $totals['total_period'],
                    'total_points' => $totals['total_points'],
                    'waiter_id' => $waiter->id,
                    'user_id' => $user->id,
                ],
        );
        $this->createCart($products, $cartRequest, $order);

        $result = [
            'order' => $order,
            'message' => 'order is added success',
        ];
        $user->increment('total_points', $totals['total_points']);

        return $result;
    }

    public function findWaiter($restaurant)
    {
        //find random waiter in current restaurant
        $waiter = $restaurant->waiters()->inRandomOrder()->firstOrFail();
        return $waiter;
    }

    public function createCart($products, $cartRequest, $order)
    {
        foreach ($products as $index => $product) {
            $order->products()->attach(
                $product->id,
                [
                    'count' => $cartRequest['order_products'][$index]['count'],
                    'price' => $product->price,
                    'details' => $cartRequest['order_products'][$index]['details'] ?? '',
                ]
            );
        }
    }
}
