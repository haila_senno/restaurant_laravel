<?php

namespace App\Http\Controllers\Api\Waiter;

use App\Enums\OrderStatusEnum;
use App\Http\Controllers\Controller;
use App\Http\Requests\Waiter\Order\ShowOrderRequest;
use App\Models\Order;
use App\Http\Requests\StoreOrderRequest;
use App\Http\Requests\Waiter\Order\UpdateOrderRequest;
use App\Http\Resources\Waiter\OrderResource;
use Illuminate\Support\Facades\Auth;
use Spatie\Permission\Commands\Show;
use Spatie\QueryBuilder\AllowedFilter;
use Spatie\QueryBuilder\QueryBuilder;

class OrderController extends Controller
{
    public function index()
    {
        // Get Data
        $waiter = Auth::guard('api_waiter')->user();
        $orders = QueryBuilder::for($waiter->orders())
                ->whereIn('status',[
                    OrderStatusEnum::PREPARING->value,
                    OrderStatusEnum::COMPLETED->value
                ])
                ->AllowedFilters('status')
                ->with(['user', 'waiter.restaurant', 'table'])
                ->orderBy('status')
                ->paginate(request('perPage'));
                $total = $orders->total();
        // Return Response
        return response()->success(
            'this is all Orders',
            [
                "orders" => OrderResource::collection($orders),
                "total" => $total
            ]
        );
    }
    public function indexPending()
    {
        // Get Data
        $waiter = Auth::guard('api_waiter')->user();
        $orders = $waiter->orders()
                ->where('status',OrderStatusEnum::PENDING->value)
                ->with(['user', 'waiter.restaurant', 'table'])
                ->latest('id')
                ->paginate(request('perPage'));
                $total = $orders->total();

        // Return Response
        return response()->success(
            'this is all Pending Orders',
            [
                "orders" => OrderResource::collection($orders),
                "total" => $total
            ]
        );
    }

    public function accept(Order $order)
    {
        $order->update(['status' => OrderStatusEnum::PREPARING->value]);
        // Return Response
        return response()->success(
            'order is accepted success'
        );
    }

    public function show(Order $order)
    {
        $order->loadMissing(['products', 'couponUser.coupon', 'waiter.restaurant', 'user', 'table']);
        // Return Response
        return response()->success(
            'this is your order',
            [
                "order" => new OrderResource($order),
            ]
        );
    }

    public function completeOrder(Order $order)
    {
        // Update Order
        $order->update(['status' => OrderStatusEnum::COMPLETED->value]);

        // Return Response
        return response()->success(
            'order is completed success'
        );
    }

}
