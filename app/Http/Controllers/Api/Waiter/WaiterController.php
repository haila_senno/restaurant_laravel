<?php

namespace App\Http\Controllers\Api\Waiter;

use App\Http\Controllers\Controller;
use App\Models\Waiter;
use App\Http\Requests\StoreWaiterRequest;
use App\Http\Requests\Waiter\Waiter\UpdateWaiterRequest;
use App\Http\Resources\Waiter\WaiterResource;
use Illuminate\Support\Facades\Auth;
use Spatie\QueryBuilder\AllowedFilter;
use Spatie\QueryBuilder\QueryBuilder;

class WaiterController extends Controller
{

    public function show()
    {
        $waiter = Auth::guard('api_waiter')->user();
        $waiter->load('media');

        // Return Response
        return response()->success(
            'this is your waiter',
            [
                "waiter" => new WaiterResource($waiter),
            ]
        );
    }

    public function update(UpdateWaiterRequest $request)
    {
        $waiter = Auth::guard('api_waiter')->user();

        // Update Waiter
         $waiter->update($request->validated());


        // Edit Image for  Waiter if exist
        $request->hasFile('image') &&
            $waiter
                ->addMediaFromRequest('image')
                ->toMediaCollection('Waiter');
        $waiter->load('media');
        // Return Response
        return response()->success(
            'waiter is updated success',
            [
                "waiter" => new WaiterResource($waiter),
            ]
        );
    }


}
