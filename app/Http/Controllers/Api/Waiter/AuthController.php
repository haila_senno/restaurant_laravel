<?php

namespace App\Http\Controllers\Api\Waiter;

use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\Waiter\LoginWaiterRequest;
use App\Http\Requests\Auth\Waiter\RegisterWaiterRequest;
use App\Http\Resources\Waiter\WaiterResource;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\Models\User;
use App\Models\Waiter;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Response;

class AuthController extends Controller
{

    public function login(LoginWaiterRequest $request): JsonResponse
    {



        $waiter = Waiter::where('username', $request->username)
            ->first();



        if (!$waiter || !Hash::check($request->password,$waiter->password))
            return Response::error('username or password is not correct');

        $token = Auth::guard('api_waiter')->attempt($request->validated());

        if (!$token)
            return Response::error('Unauthorized', '401');

        $waiter = Auth::guard('api_waiter')->user();
        return Response::success(
            'waiter login success',
            [
                "token" => $token,
                "waiter" => new WaiterResource($waiter),
            ]
        );
    }

    public function logout()
    {
        Auth::guard('api_waiter')->logout();
        return response()->json([
            'status' => 'success',
            'message' => 'Successfully logged out',
        ]);
    }
    public function refresh()
    {
        return response()->json([
            'status' => 'success',
            'user' => Auth::user(),
            'authorisation' => [
                'token' => Auth::refresh(),
                'type' => 'bearer',
            ]
        ]);
    }
}
