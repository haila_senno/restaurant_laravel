<?php

namespace App\Http\Controllers\Api\User;

use App\Enums\OrderStatusEnum;
use App\Http\Controllers\Controller;
use App\Http\Requests\User\Order\StoreOrderRequest;
use App\Http\Requests\User\Cart\StoreCartRequest;
use App\Http\Resources\User\CartResource;
use App\Http\Resources\User\OrderResource;
use App\Models\Coupon;
use App\Models\Order;
use App\Models\Product;
use App\Models\Restaurant;
use App\Models\Waiter;
use App\Services\OrderService;
use App\Services\SettingService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Spatie\QueryBuilder\AllowedFilter;
use Spatie\QueryBuilder\QueryBuilder;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::guard('api_user')->user();

        $orders = $user->orders()
            ->completed()
            ->with(['user', 'waiter.restaurant', 'table', 'couponUser.coupon'])
            ->latest('id')
            ->paginate(request('perPage', 5));

        $total = $orders->total();

        return response()->success(
            'this is all orders',
            [

                'products' => OrderResource::collection($orders),
                'total' => $total,

            ]
        );
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreOrderRequest $orderRequest, StoreCartRequest $cartRequest, Restaurant $restaurant)
    {
        $user = Auth::guard('api_user')->user();
        $checkTable = (new OrderService)->checkOrdersTables($user, $orderRequest->table_id, $restaurant);
        $checkPending = (new OrderService)->checkPendingOrders($user);
        if ($checkPending)
            return response()->error('you have already an order in pending status');
        if ($checkTable)
            return response()->error('you have already an order for this table in preparing status');
        $result = (new OrderService)->storeOrder(
            $orderRequest->validated(),
            $cartRequest->validated(),
            $orderRequest->couponValidated(),
            $user,
            $restaurant,
        );

        if ($result['order'] == null)
            return response()->error($result['message']);
        else
            $result['order']->loadMissing(['user', 'waiter.restaurant', 'table', 'couponUser.coupon']);
        return response()->success(
            $result['message'],
            [
                "order" => new OrderResource($result['order']),
            ]
        );


        // Return Response
        return response()->success(
            'order is added success',
            [
                "order" => new OrderResource($result['order']),
            ]
        );
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function showCart(Order $order)
    {
        $carts = $order->products()
            ->with('media')->get();
        return response()->success(
            'this is your cart ',
            [
                "cart" => CartResource::collection($carts),
            ]
        );
    }

    public function cancel(Order $order)
    {
        $order->update(['status' => OrderStatusEnum::CANCELED->value]);
        return response()->success(
            'your order is canceled success'
        );
    }
}
