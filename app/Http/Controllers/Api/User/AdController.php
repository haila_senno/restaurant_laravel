<?php

namespace App\Http\Controllers\Api\User;

use App\Enums\OrderStatusEnum;
use App\Http\Controllers\Controller;
use App\Http\Resources\User\AdResource;
use App\Models\Ad;
use App\Models\Order;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;

class AdController extends Controller
{
    public function index()
    {
        // Get Data
        $user = Auth::guard('api_user')->user();

        // i didn't use whereIn in with function because i want the restaurant name for all ads not only
        // for restaurant that belongs to the auth user
        $ads = Ad::active()->with('restaurant')
            ->orderByRaw("FIELD(restaurant_id, {$user->restaurants->pluck('id')->implode(',')}) DESC")
            ->latest('id')
            ->paginate(request('perPage', 5));
        $order = $user->orders()
            ->whereDate('created_at', Carbon::today())->latest('id')->first();

        // dd($ads);

        $total = $ads->total();

        // Return Response
        return response()->success(
            'this is all ads',
            [
                'orderStatus' => $order?->status ?? "",
                'ads' => AdResource::collection($ads),
                'total' => $total,
            ]
        );
    }

    public function show(Ad $ad)
    {
        $ad->load('restaurant');

        // Return Response
        return response()->success(
            'this is your ad',
            [
                'ad' => new AdResource($ad),
            ]
        );
    }
}
