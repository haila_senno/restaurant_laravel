<?php

namespace App\Http\Controllers\Api\User;

use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\User\LoginUserRequest;
use App\Http\Requests\Auth\User\RegisterUserRequest;
use App\Http\Resources\User\RestaurantResource;
use App\Http\Resources\User\UserResource;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\Models\User;
use App\Services\CodeGenerator;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Response;

class AuthController extends Controller
{


    public function login(LoginUserRequest $request): JsonResponse
    {

        $user = User::where('username', $request->username)
            ->first();


        if (!$user  || !Hash::check($request->password, $user->password))
            return Response::error('username or password is not correct');
        $token = Auth::guard('api_user')->attempt($request->validated());

        if (!$token)
            return Response::error('Unauthorized', '401');

        $user = Auth::guard('api_user')->user();
        $restaurants = $user->restaurants()->where('user_id',$user->id)->get();
        // dd($restaurants);
        return Response::success(
            'user login success',
            [
                "token" => $token,
                "user" => new UserResource($user),
                'restaurants' => RestaurantResource::collection($restaurants),
            ]
        );
    }
    public function register(RegisterUserRequest $request): JsonResponse
    {
        // Generate a unique QR code
        // $qrCode = CodeGenerator::generateUniqueCode(User::class, 'user_code', 8);

        $user = User::create($request->validated());

        $user
        ->addMediaFromRequest('image')
        ->toMediaCollection('Admin');

        $token = Auth::guard('api_user')->login($user);
        return Response::success(
            'user register success',
            [
                "token" => $token,
                "user" => new UserResource($user),
            ]
        );
    }

    public function logout()
    {
        Auth::guard('api_user')->logout();
        return response()->json([
            'status' => 'success',
            'message' => 'Successfully logged out',
        ]);
    }

    // public function refresh()
    // {
    //     return response()->json([
    //         'status' => 'success',
    //         'user' => Auth::user(),
    //         'authorisation' => [
    //             'token' => Auth::refresh(),
    //             'type' => 'bearer',
    //         ]
    //     ]);
    // }
}
