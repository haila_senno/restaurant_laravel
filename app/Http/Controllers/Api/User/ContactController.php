<?php

namespace App\Http\Controllers\Api\User;

use App\Http\Controllers\Controller;
use App\Models\Contact;
use App\Http\Requests\Admin\Contact\StoreContactRequest;
use App\Http\Requests\Admin\Contact\UpdateContactRequest;
use App\Http\Resources\User\ContactResource;
use Spatie\QueryBuilder\AllowedFilter;
use Spatie\QueryBuilder\QueryBuilder;

class ContactController extends Controller
{
    public function index()
    {
        // Get Data
        $contacts = Contact::orderBy('priority')
            ->paginate(request('perPage'));
        // Return Response
        return response()->success(
            'this is all Contacts',
            [
                "contacts" => ContactResource::collection($contacts),
            ]
        );
    }
}
