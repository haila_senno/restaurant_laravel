<?php

namespace App\Http\Controllers\Api\User;

use App\Http\Controllers\Controller;
use App\Http\Resources\NotificationResource;
use App\Models\Notification;
use App\Http\Requests\StoreNotificationRequest;
use App\Http\Requests\UpdateNotificationRequest;

use Spatie\QueryBuilder\AllowedFilter;
use Spatie\QueryBuilder\QueryBuilder;

class NotificationController extends Controller
{
    public function index()
    {
        // Get Data
        $notifications = Notification::latest()->get();

        // OR with filter

        // $notifications = QueryBuilder::for(Notification::class)
        //     ->allowedFilters([
        //         "test_id",
        //         AllowedFilter::exact('test_id'),
        //     ])->get();


        // Return Response
        return response()->success(
            'this is all Notifications',
            [
                "notifications" => NotificationResource::collection($notifications),
            ]
        );
    }


    public function store(StoreNotificationRequest $request)
    {
        // Store Notification
        $notification = Notification::create($request->validated());


        // Add Image to Notification
        $notification
            ->addMediaFromRequest('image')
            ->toMediaCollection('Notification');

        // Return Response
        return response()->success(
            'notification is added success',
            [
                "notification" => new NotificationResource($notification),
            ]
        );
    }


    public function show(Notification $notification)
    {
        // Return Response
        return response()->success(
            'this is your notification',
            [
                "notification" => new NotificationResource($notification),
            ]
        );
    }

    public function update(UpdateNotificationRequest $request, Notification $notification)
    {
        // Update Notification
         $notification->update($request->validated());


        // Edit Image for  Notification if exist
        $request->hasFile('image') &&
            $notification
                ->addMediaFromRequest('image')
                ->toMediaCollection('Notification');



        // Return Response
        return response()->success(
            'notification is updated success',
            [
                "notification" => new NotificationResource($notification),
            ]
        );
    }

    public function destroy(Notification $notification)
    {
        // Delete Notification
        $notification->delete();

        // Return Response
        return response()->success('notification is deleted success');
    }
}
