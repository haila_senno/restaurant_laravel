<?php

namespace App\Http\Controllers\Api\User;

use App\Http\Controllers\Controller;
use App\Http\Requests\User\User\UpdateUserRequest;
use App\Http\Resources\User\UserResource;
use App\Http\Resources\Waiter\OrderResource;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Spatie\QueryBuilder\QueryBuilder;

class UserController extends Controller
{
    public function show()
    {
        $user = Auth::guard('api_user')->user();

        return response()->success(
            'this is your user',
            [
                "user" => new UserResource($user),
            ]
        );
    }

    public function update(UpdateUserRequest $request)
    {
        $user = Auth::guard('api_user')->user();
        $user->update($request->validated());

        $request->hasFile('image') &&
        $user
        ->addMediaFromRequest('image')
        ->toMediaCollection('User');
        return response()->success(
            'user is updated success',
            [
                "user" => new UserResource($user),
            ]
        );
    }

    public function getOrders()
    {
        $user = Auth::guard('api_user')->user();
        $orders = QueryBuilder::for($user->orders())
            ->with(['waiter.restaurant'])
            ->allowedFilters(['status', 'waiter.restaurant.name'])
            ->paginate(request('perPage', 5));
        $total = $orders->total();



        return response()->success(
            'this is all orders',
            [
                "orders" => OrderResource::collection($orders),
                'total' => $total
            ]
        );
    }
}
