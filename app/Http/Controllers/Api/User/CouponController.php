<?php

namespace App\Http\Controllers\Api\User;

use App\Http\Controllers\Controller;
use App\Http\Resources\User\CouponResource;
use App\Models\Coupon;
use App\Http\Requests\StoreCouponRequest;
use App\Http\Requests\UpdateCouponRequest;
use Illuminate\Support\Facades\Auth;
use Spatie\QueryBuilder\AllowedFilter;
use Spatie\QueryBuilder\QueryBuilder;

class CouponController extends Controller
{
    public function index()
    {
        // Get Data
        $user = Auth::guard('api_user')->user();
        $coupons = Coupon::isAvailable()
            ->latest('id')
            ->paginate(request('perPage', 5));
        $total = $coupons->total();
        // Return Response
        return response()->success(
            'this is all Coupons',
            [
                "user_points" => $user->total_points,
                "coupons" => CouponResource::collection($coupons),
                "total" => $total,
            ]
        );
    }

    public function userCoupons()
    {
        // Get Data
        $user = Auth::guard('api_user')->user();
        $coupons = $user->coupons()
                    ->latest('id')
                    ->paginate(request('perPage',5));

        $total = $coupons->total();
        // Return Response
        return response()->success(
            'this is all user coupons',
            [
                "user_points" => $user->total_points,
                "coupons" => CouponResource::collection($coupons),
                "total" => $total,
            ]
        );
    }

    public function buyCoupon(Coupon $coupon)
    {
        // Store CouponUser
        $user = Auth::guard('api_user')->user();
        $checkResult = $user->checkPoints($coupon);
        if ($checkResult) {

            $result =  $user->coupons()->syncWithoutDetaching($coupon->id);
            if (!empty($result['attached'])) {
                $user->update([
                    'total_points' => $user->total_points - $coupon->points
                ]);
                return response()->success(
                    'coupon is bought success',
                    [
                        'user_points' => $user->total_points
                    ]
                );
            } else {
                return response()->error('you have bought this coupon before');
            }
        } else {
            return response()->error('you do not have enough points');
        }

    }



    public function show(Coupon $coupon)
    {
        // Return Response
        return response()->success(
            'this is your coupon',
            [
                "coupon" => new CouponResource($coupon),
            ]
        );
    }
}
