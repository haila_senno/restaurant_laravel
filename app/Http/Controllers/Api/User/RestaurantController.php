<?php

namespace App\Http\Controllers\Api\User;

use App\Http\Controllers\Controller;
use App\Http\Resources\User\ProductResource;
use App\Http\Resources\User\RestaurantResource;
use App\Models\Category;
use App\Models\Restaurant;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Spatie\QueryBuilder\QueryBuilder;

class RestaurantController extends Controller
{
    public function index()
    {
        $user = Auth::guard('api_user')->user();
        $restaurants = QueryBuilder::for(Restaurant::class)
            ->allowedFilters('name')
            ->latest('id')
            ->with(['media','tables'])
            ->paginate(request('perPage', 5));

        $total = $restaurants->total();


        return response()->success(
            'this is all restaurants',
            [
                'user_points' => $user->total_points,
                'restaurants' => RestaurantResource::collection($restaurants),
                'total' => $total,

            ]
        );
    }

    public function show(Restaurant $restaurant)
    {
        $restaurant->load(['media','tables','owner']);

        return response()->success(
            'this is your restaurant',
            [
                'restaurant' => new RestaurantResource($restaurant),
            ]
        );
    }

    public function toggleSubscribe(Restaurant $restaurant)
    {
        // Store RestaurantUser
        $user = Auth::guard('api_user')->user();
        if ($user->restaurants()->where('restaurant_id', $restaurant->id)->exists()) {
            $user->restaurants()->detach($restaurant->id);
            $message = 'the user is unsubscribed from restaurant success';
        } else {
            $restaurant->users()->syncWithoutDetaching($user->id);
            $user->update(['total_points' => $user->total_points + 10]);
            $message = "the user is subscribed to restaurant success";
        }

        return response()->success(
            $message,
            [
                'total_points' => $user->total_points,
            ]
        );
    }

}
