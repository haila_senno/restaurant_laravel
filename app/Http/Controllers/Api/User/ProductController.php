<?php

namespace App\Http\Controllers\Api\User;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreRestaurantCodeRequest;
use App\Models\Product;
use App\Http\Resources\User\ProductResource;
use App\Models\Category;
use App\Models\Restaurant;
use App\Providers\SettingServiceProvider;
use Spatie\QueryBuilder\AllowedFilter;
use Spatie\QueryBuilder\QueryBuilder;

class ProductController extends Controller
{
    public function index(Restaurant $restaurant)
    {
        $products = QueryBuilder::for($restaurant->products()->isShown())
            ->allowedFilters(
                AllowedFilter::exact('category_id', 'categoryRestaurant.category.id'),
            )
            ->with(['media', 'categoryRestaurant.category', 'categoryRestaurant.restaurant'])
            ->get()
            ->groupBy('categoryRestaurant.category.id')
            ->map(function ($products) {
                return [
                    'category_name' => $products->first()->categoryRestaurant->category->name,
                    'products' => ProductResource::collection($products)
                ];
            })
            // ->paginate(request('perPage', 5))
            ->values();


        // $total = $products->total();

        return response()->success(
            'this is all products',
            [

                'products' => $products,
                // 'total' => $total,

            ]
        );
    }

    public function show(Product $product)
    {
        $product->load(['media', 'categoryRestaurant.category', 'categoryRestaurant.restaurant']);
        return response()->success(
            'this is your product',
            [

                'products' => new ProductResource($product),

            ]
        );
    }
}
