<?php

namespace App\Http\Controllers\Api\Admin;

use App\Enums\AdminPermissionEnum;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Category\StoreCategoryRequest;
use App\Http\Requests\Admin\Category\UpdateCategoryRequest;
use App\Models\Category;
use App\Http\Resources\Admin\CategoryResource;
use App\Models\Restaurant;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;
use Spatie\QueryBuilder\AllowedFilter;
use Spatie\QueryBuilder\QueryBuilder;

class CategoryController extends Controller
{
    public function index(Restaurant $restaurant)
    {

        $admin = Auth::guard('api_admin')->user();
        // Authorize the action
        $this->authorize('viewAny', [Category::class, $restaurant]);
        $categories = QueryBuilder::for($restaurant->categories())
            ->withCount('products')
            ->allowedFilters('name')
            ->latest('id')
            ->paginate(request("perPage", 5));
        $total = $categories->total();


        // Return Response
        return response()->success(
            'this is all categories',
            [
                "categories" => CategoryResource::collection($categories),
                "total" => $total,
            ]
        );
    }

    public function show(Category $category)
    {
        $admin = Auth::guard('api_admin');
        $this->authorize('view', [Category::class, $category]);
        // Delete Category
        // Return Response
        return response()->success(
            'this  is your category',
            [
                "category" => new CategoryResource($category),
            ]
        );
    }

    public function store(StoreCategoryRequest $request, Restaurant $restaurant)
    {
        // Store Category
        $admin = Auth::guard('api_admin');
        $this->authorize('create', [Category::class, $restaurant]);
        $category = Category::create($request->validated());
        $category->restaurants()->attach($restaurant->id);

        // Return Response
        return response()->success(
            'category is added success',
            [
                "category" => new CategoryResource($category),
            ]
        );
    }

    public function update(UpdateCategoryRequest $request, Category $category)
    {
        // Update Category
        // dd($request->validated());
        $category->update($request->validated());
        $category
            ->loadCount('products');

        // Return Response
        return response()->success(
            'category is updated success',
            [
                "category" => new CategoryResource($category),
            ]
        );
    }

    public function destroy(Category $category)
    {
        $admin = Auth::guard('api_admin');
        $this->authorize('delete', [Category::class, $category]);
        // Delete Category
        $category->delete();
        // Return Response
        return response()->success('category is deleted success');
    }
}
