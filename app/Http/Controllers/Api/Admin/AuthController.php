<?php

namespace App\Http\Controllers\Api\Admin;

use App\Enums\AdminRoleEnum;
use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\Admin\LoginAdminRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\Auth\Admin\RegisterAdminRequest;
use App\Http\Resources\Admin\AdminResource;
use App\Models\Admin;
use Illuminate\Support\Facades\Hash;
use App\Models\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Response;

class AuthController extends Controller
{

    public function login(LoginAdminRequest $request): JsonResponse
    {
        $admin = Admin::where('username', $request->username)
            ->first();


        if (!$admin  || !Hash::check($request->password, $admin->password))
            return Response::error('username or password is not correct');

        $token = Auth::guard('api_admin')->attempt($request->validated());
        if (!$token) {
            return Response::error('Unauthorized', '401');
        }
        $admin = Auth::guard('api_admin')->user();
        return Response::success(
            'admin login success',
            [
                "token" => $token,
                "admin" => new AdminResource($admin),
            ]
        );
    }

    public function registerAdmin(RegisterAdminRequest $request): JsonResponse
    {

        $admin = Admin::create($request->validated())
            ->assignRole(AdminRoleEnum::ADMIN->value);
        //dd($admin);

        $admin
            ->addMediaFromRequest('image')
            ->toMediaCollection('Admin');

        $token = Auth::guard('api_admin')->login($admin);
        return Response::success(
            'admin register success',
            [
                "token" => $token,
                "admin" => new AdminResource($admin),
            ]
        );
    }



    public function logout()
    {
        Auth::guard('api_admin')->logout();
        return response()->success([
            'status' => 'success',
            'message' => 'Successfully logged out',
        ]);
    }

    // public function refresh()
    // {
    //     return response()->json([
    //         'status' => 'success',
    //         'user' => Auth::user(),
    //         'authorisation' => [
    //             'token' => Auth::refresh(),
    //             'type' => 'bearer',
    //         ]
    //     ]);
    // }

}
