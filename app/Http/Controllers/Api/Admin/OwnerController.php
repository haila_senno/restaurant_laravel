<?php

namespace App\Http\Controllers\Api\Admin;

use App\Enums\AdminRoleEnum;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Owner\StoreOwnerRequest;
use App\Http\Requests\Admin\Owner\UpdateOwnerRequest;
use App\Http\Resources\Admin\AdminResource;
use App\Http\Resources\Admin\RestaurantResource;
use App\Models\Admin;
use App\Models\Restaurant;
use Illuminate\Http\Request;
use Spatie\QueryBuilder\QueryBuilder;

class OwnerController extends Controller
{

    public function index()
    {
        $owners = QueryBuilder::for(Admin::owners())
            ->with('media')
            ->allowedFilters('name')
            ->latest('id')
            ->paginate(request('perPage', 5));

        $total = $owners->total();

        return response()->success(
            'this is all owners',
            [
                'owners' => AdminResource::collection($owners),
                'total' => $total
            ]
        );
    }

    public function store(StoreOwnerRequest $request)
    {
        // Store Owner
        $owner = Admin::create($request->validated())
            ->assignRole(AdminRoleEnum::OWNER->value);

        $owner
            ->addMediaFromRequest('image')
            ->toMediaCollection('Admin');

        $owner->load('media');

        // Return Response
        return response()->success(
            'owner is added success',
            [
                "owner" => new AdminResource($owner),
            ]
        );
    }

    public function update(UpdateOwnerRequest $request, Admin $owner)
    {
        // Store Owner
        $owner->update($request->validated());

        $request->hasFile('image') &&
            $owner->addMediaFromRequest('image')
            ->toMediaCollection('Admin');

        $owner->load('media');

        // Return Response
        return response()->success(
            'owner is updated success',
            [
                "owner" => new AdminResource($owner),
            ]
        );
    }

    public function show(Admin $owner)
    {
        $owner->load('media');
        // Return Response
        return response()->success(
            'this is your owner',
            [
                "owner" => new AdminResource($owner),
            ]
        );
    }



    public function destroy(Admin $owner)
    {
        $owner->delete();
        return response()->success(
            'owner is deleted success'
        );
    }

    public function getRestaurants(Admin $admin)
    {
        $restaurants = QueryBuilder::for($admin->restaurants())
            ->with('owner:id,name')
            ->allowedFilters('name')
            ->latest()
            ->paginate(request('perPage', 5));
        $total = $restaurants->total();
        // Return Response
        return response()->success(
            'this is all restaurants',
            [
                "restaurants" => RestaurantResource::collection($restaurants),
                'total' => $total
            ]
        );
    }
}
