<?php

namespace App\Http\Controllers\Api\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Table\StoreTableRequest;
use App\Http\Requests\Admin\Table\UpdateTableRequest;
use App\Models\Table;
use App\Http\Resources\Admin\TableResource;
use App\Models\Restaurant;
use Illuminate\Support\Facades\Auth;
use Spatie\QueryBuilder\AllowedFilter;
use Spatie\QueryBuilder\QueryBuilder;

class TableController extends Controller
{
    public function index(Restaurant $restaurant)
    {
        // Get Data
        $admin = Auth::guard('api_admin')->user();
        $this->authorize('viewAny', [Table::class, $restaurant]);
        $tables = $restaurant->tables()
            ->with('restaurant:id,name')
            ->latest()
            ->paginate(request('perPage', 5));
        $total = $tables->total();
        // Return Response
        return response()->success(
            'this is all Tables',
            [
                "tables" => TableResource::collection($tables),
                'total' => $total

            ]
        );
    }


    public function store(StoreTableRequest $request, Restaurant $restaurant)
    {
        // Store Table
        $admin = Auth::guard('api_admin')->user();
        $this->authorize('create', [Table::class, $restaurant]);
        $table = $restaurant->tables()->create($request->validated());
        // Return Response
        return response()->success(
            'table is added success',
            [
                "table" => new TableResource($table),
            ]
        );
    }


    public function show(Table $table)
    {
        $admin = Auth::guard('api_admin')->user();
        $this->authorize('view', [Table::class, $table]);

        $table->load('restaurant:id,name');
        // Return Response
        return response()->success(
            'this is your table',
            [
                "table" => new TableResource($table),
            ]
        );
    }

    public function update(UpdateTableRequest $request, Table $table)
    {
        // Update Table
        $table->update($request->validated());
        //dd($table);

        $table->load('restaurant');
        // Return Response
        return response()->success(
            'table is updated success',
            [
                "table" => new TableResource($table),
            ]
        );
    }

    public function destroy(Table $table)
    {
        // Delete Table
        $admin = Auth::guard('api_admin')->user();
        $this->authorize('delete', [Table::class, $table]);
        $table->delete();

        // Return Response
        return response()->success('table is deleted success');
    }
}
