<?php

namespace App\Http\Controllers\Api\Admin;

use App\Enums\AdminRoleEnum;
use App\Actions\GetRestaurantsAction;
use App\Http\Controllers\Controller;
use App\Models\Restaurant;
use App\Http\Requests\Admin\Restaurant\StoreRestaurantRequest;
use App\Http\Requests\Admin\Restaurant\UpdateRestaurantRequest;
use App\Http\Resources\Admin\RestaurantResource;
use App\Models\Admin;
use Illuminate\Support\Facades\Auth;
use Spatie\QueryBuilder\AllowedFilter;
use Spatie\QueryBuilder\QueryBuilder;

class RestaurantController extends Controller
{
    public function index()
    {
        $admin = Auth::guard('api_admin')->user();
        // Authorize the action
        $this->authorize('viewAny', Restaurant::class);

        $restaurants = (new GetRestaurantsAction)->execute($admin)
                        ->paginate(request('perPage', 5));

        $total = $restaurants->total();

        // Return Response
        return response()->success(
            'this is all Restaurants',
            [
                "restaurants" => RestaurantResource::collection($restaurants),
                'total' => $total

            ]
        );
    }


    public function store(StoreRestaurantRequest $request)
    {
        // Store Restaurant
        $restaurant = Restaurant::create($request->validated());

        // Add Image to Restaurant
        $restaurant
            ->addMediaFromRequest('image')
            ->toMediaCollection('Restaurant');

        // Return Response
        return response()->success(
            'restaurant is added success',
            [
                "restaurant" => new RestaurantResource($restaurant),
            ]
        );
    }


    public function show(Restaurant $restaurant)
    {
        // Return Response
        $admin = Auth::guard('api_admin')->user();
        $this->authorize('view', [Restaurant::class,$restaurant]);

        $restaurant->load('owner')
            ->loadCount(['products', 'categories']);
        return response()->success(
            'this is your restaurant',
            [
                "restaurant" => new RestaurantResource($restaurant),
            ]
        );
    }

    public function update(UpdateRestaurantRequest $request, Restaurant $restaurant)
    {
        $admin = Auth::guard('api_admin')->user();
        // Authorize the action
        $this->authorize('update', [Restaurant::class,$restaurant]);

        $restaurant->update($request->validated());

        $restaurant->load('owner')
        ->loadCount(['products', 'categories']);

        // Edit Image for  Restaurant if exist
        $request->hasFile('image') &&
            $restaurant
            ->addMediaFromRequest('image')
            ->toMediaCollection('Restaurant');



        // Return Response
        return response()->success(
            'restaurant is updated success',
            [
                "restaurant" => new RestaurantResource($restaurant),
            ]
        );
    }

    public function destroy(Restaurant $restaurant)
    {
        // Delete Restaurant
        $restaurant->delete();

        // Return Response
        return response()->success('restaurant is deleted success');
    }
}
