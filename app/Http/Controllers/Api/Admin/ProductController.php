<?php

namespace App\Http\Controllers\Api\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Product\StoreProductRequest;
use App\Http\Requests\Admin\Product\UpdateProductRequest;
use App\Http\Resources\Admin\ProductResource;
use App\Models\Category;
use App\Models\CategoryRestaurant;
use App\Models\Product;
use App\Models\Restaurant;
use App\Models\Setting;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;
use Spatie\QueryBuilder\AllowedFilter;
use Spatie\QueryBuilder\QueryBuilder;

class ProductController extends Controller
{
    public function index(Restaurant $restaurant, Category $category)
    {
        // Get Data
        //dd($restaurant);
        $admin = Auth::guard('api_admin')->user();
        $this->authorize('viewAny', [Product::class, $restaurant]);
        $pivot = $restaurant->categories()
            ->where('categories.id', $category->id)
            ->firstOrFail()
            ->pivot;
        $products = QueryBuilder::for($pivot->products())
            ->with('media')
            ->allowedFilters([
                'name',
                AllowedFilter::exact('price')
            ])
            ->latest('id')
            ->paginate(request('perPage', 5));

        $total = $products->total();

        // Return Response
        return response()->success(
            'this is all Products',
            [
                'products' => ProductResource::collection($products),
                'total' => $total,
            ]
        );
    }

    public function store(StoreProductRequest $request, Restaurant $restaurant, Category $category)
    {
        // Store Product
        $admin = Auth::guard('api_admin')->user();
        $this->authorize('create', [Product::class, $restaurant]);

        $product_points = Setting::getPoints($request->price);
        $pivot = $restaurant->categories()
            ->where('categories.id', $category->id)
            ->firstOrFail()
            ->pivot;
        $product = $pivot->products()->create(
            [
                ...$request->validated(),
                'points' => $product_points,
            ]

        );

        // Add Image to Product
        $product
            ->addMediaFromRequest('image')
            ->toMediaCollection('Product');

        $product->load('media');

        // Return Response
        return response()->success(
            'product is added success',
            [
                'product' => new ProductResource($product),
            ]
        );
    }

    public function show(Product $product)
    {

        // Return Response
        $admin = Auth::guard('api_admin')->user();
        $this->authorize('view', [Product::class, $product]);
        $product->load('media');

        return response()->success(
            'this is your product',
            [
                'product' => new ProductResource($product),
            ]
        );
    }

    public function update(UpdateProductRequest $request, Product $product)
    {
        // Update Product
        if ($request->has('price')) {
            $product_points = Setting::getPoints($request->price);
            $product->update([
                ...$request->validated(),
                'points' => $product_points,
            ]);
        } else
            $product->update(
                $request->validated()
            );



        // Edit Image for  Product if exist
        $request->hasFile('image') &&
            $product
            ->addMediaFromRequest('image')
            ->toMediaCollection('Product');




        $product->load('media');

        // Return Response
        return response()->success(
            'product is updated success',
            [
                'product' => new ProductResource($product),
            ]
        );
    }

    public function destroy(Product $product)
    {
        // Delete Product
        // Delete Table
        $admin = Auth::guard('api_admin')->user();
        $this->authorize('delete', [Product::class, $product]);
        $product->delete();

        // Return Response
        return response()->success('product is deleted success');
    }
}
