<?php

namespace App\Http\Controllers\Api\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Order\UpdateOrderRequest;
use App\Http\Resources\Admin\OrderResource;
use App\Models\Order;
use App\Models\User;
use Illuminate\Http\Request;
use Spatie\QueryBuilder\AllowedFilter;
use Spatie\QueryBuilder\QueryBuilder;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $orders = QueryBuilder::for(Order::class)
            ->with('waiter.restaurant', 'user', 'table')
            ->allowedFilters([
                AllowedFilter::exact('waiter_name', 'waiter.name'),
                AllowedFilter::exact('restaurant_name', 'waiter.restaurant.name'),
                AllowedFilter::exact('user_name', 'user.name'),
                AllowedFilter::exact('status'),
            ])
            ->latest('id')
            ->paginate(request('perPage', 5));
        $total = $orders->total();

        return response()->success(
            'this is all orders',
            [
                'orders' => OrderResource::collection($orders),
                'total' => $total
            ]
        );
    }


    public function indexUser(User $user)
    {
        $orders = QueryBuilder::for($user->orders())
            ->with('waiter.restaurant', 'user', 'table')
            ->allowedFilters([
                AllowedFilter::exact('waiter_name', 'waiter.name'),
                AllowedFilter::exact('restaurant_name', 'waiter.restaurant.name'),
                AllowedFilter::exact('status'),
            ])
            ->latest('id')
            ->paginate(request('perPage', 5));
        $total = $orders->total();

        return response()->success(
            'this is all orders',
            [
                'orders' => OrderResource::collection($orders),
                'total' => $total
            ]
        );
    }



    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Order $order)
    {
        $order->loadMissing(['products', 'couponUser.coupon', 'waiter.restaurant', 'user', 'table']);

        return response()->success(
            'this is your order',
            [
                'order' => new OrderResource($order)
            ]
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateOrderRequest $request, Order $order)
    {
        $order->update($request->validated());
        $order->loadMissing(['products', 'couponUser.coupon', 'waiter.restaurant', 'user', 'table']);

        return response()->success(
            'this is your order',
            [
                'order' => new OrderResource($order)
            ]
        );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
