<?php

namespace App\Http\Controllers\Api\Admin;

use App\Enums\AdminRoleEnum;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Admin\StoreAdminRequest;
use App\Http\Requests\Admin\Admin\UpdateAdminRequest;
use App\Models\Admin;
use App\Http\Resources\Admin\AdminResource;
use App\Http\Resources\Admin\RestaurantResource;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;
use Spatie\QueryBuilder\AllowedFilter;
use Spatie\QueryBuilder\QueryBuilder;

class AdminController extends Controller
{
    protected $authAdmin;
    public function __construct()
    {
        $this->authAdmin = Auth::guard('api_admin')->user();
    }
    public function index()
    {
        // Get Data
        $admins = Admin::latest('id')->get();

        // OR with filter

        $admins = QueryBuilder::for(Admin::class)
            ->allowedFilters([
                'name',
                AllowedFilter::exact('role_name', 'roles.id'),
            ])->get();


        // Return Response
        return response()->success(
            'this is all Admins',
            [
                "admins" => AdminResource::collection($admins),
            ]
        );
    }

    public function getRestaurants(Admin $admin)
    {
        $restaurants = QueryBuilder::for($admin->restaurants())
            ->with('owner:id,name')
            ->allowedFilters('name')
            ->latest()
            ->paginate(request('perPage', 5));
        $total = $restaurants->total();
        // Return Response
        return response()->success(
            'this is all restaurants',
            [
                "restaurants" => RestaurantResource::collection($restaurants),
                'total' => $total
            ]
        );
    }


    public function storeOwner(storeAdminRequest $request)
    {
        // Store Admin
        $admin = Admin::create($request->validated())->assignRole(AdminRoleEnum::OWNER->value);


        // Add Image to Admin
        // $admin
        //     ->addMediaFromRequest('image')
        //     ->toMediaCollection('Admin');

        // Return Response
        return response()->success(
            'owner is added success',
            [
                "admin" => new AdminResource($admin),
            ]
        );
    }


    public function show()
    {
        // Return Response
        $this->authAdmin->load('media');
        return response()->success(
            'this is your admin',
            [
                "admin" => new AdminResource($this->authAdmin),
            ]
        );
    }

    public function update(UpdateAdminRequest $request)
    {
        // Update Admin
        $this->authAdmin->update($request->validated());

        $this->authAdmin->load('media');

        // Edit Image for  Admin if exist
        $request->hasFile('image') &&
            $this->authAdmin
            ->addMediaFromRequest('image')
            ->toMediaCollection('Admin');



        // Return Response
        return response()->success(
            'admin is updated success',
            [
                "admin" => new AdminResource($this->authAdmin),
            ]
        );
    }

    public function destroy(Admin $admin)
    {

        Gate::denyIf($this->authAdmin->hasRole(AdminRoleEnum::OWNER->value));

        // Delete Admin
        $admin->delete();

        // Return Response
        return response()->success('admin is deleted success');
    }
}
