<?php

namespace App\Http\Controllers\Api\Admin;

use App\Actions\GetWaitersAction;
use App\Enums\AdminRoleEnum;
use App\Http\Controllers\Controller;
use App\Models\Waiter;
use App\Http\Requests\Admin\Waiter\StoreWaiterRequest;
use App\Http\Requests\Admin\Waiter\UpdateWaiterRequest;
use App\Http\Resources\Admin\WaiterResource;
use App\Models\Restaurant;
use Illuminate\Support\Facades\Auth;
use Spatie\QueryBuilder\AllowedFilter;
use Spatie\QueryBuilder\QueryBuilder;

class WaiterController extends Controller
{
    public function index()
    {
        // Get Data
        $admin = Auth::guard('api_admin')->user();

        $this->authorize('viewAny', Waiter::class);

        $waiters = (new GetWaitersAction)->execute($admin)
            ->paginate(request('perPage', 5));

        $total = $waiters->total();


        // Return Response
        return response()->success(
            'this is all Waiters',
            [
                "waiters" => WaiterResource::collection($waiters),
                'total' => $total
            ]
        );
    }


    public function store(StoreWaiterRequest $request)
    {
        // Store Waiter

        $waiter = Waiter::create($request->validated());


        // // Add Image to Waiter
        $waiter
            ->addMediaFromRequest('image')
            ->toMediaCollection('Waiter');

        $waiter->load(['restaurant', 'media']);
        // Return Response
        return response()->success(
            'waiter is added success',
            [
                "waiter" => new WaiterResource($waiter),
            ]
        );
    }


    public function show(Waiter $waiter)
    {
        $admin = Auth::guard('api_admin')->user();
        $this->authorize('view', [Waiter::class, $waiter]);

        $waiter->load(['restaurant', 'media']);
        return response()->success(
            'this is your waiter',
            [
                "waiter" => new WaiterResource($waiter),
            ]
        );
    }

    public function update(UpdateWaiterRequest $request, Waiter $waiter)
    {
        // Update Waiter
        $waiter->update($request->validated());


        // Edit Image for  Waiter if exist
        $request->hasFile('image') &&
            $waiter
            ->addMediaFromRequest('image')
            ->toMediaCollection('Waiter');


        $waiter->load(['restaurant', 'media']);

        // Return Response
        return response()->success(
            'waiter is updated success',
            [
                "waiter" => new WaiterResource($waiter),
            ]
        );
    }

    public function destroy(Waiter $waiter)
    {
        // Delete Waiter
        $admin = Auth::guard('api_admin')->user();
        $this->authorize('delete', [Waiter::class, $waiter]);
        $waiter->delete();

        // Return Response
        return response()->success('waiter is deleted success');
    }
}
