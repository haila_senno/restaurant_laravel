<?php

namespace App\Http\Controllers\Api\Admin;

use App\Actions\GetAdsAction;
use App\Http\Controllers\Controller;
use App\Http\Resources\Admin\AdResource;
use App\Models\Ad;
use App\Http\Requests\Admin\Ad\StoreAdRequest;
use App\Http\Requests\Admin\Ad\UpdateAdRequest;
use App\Models\Restaurant;
use Illuminate\Support\Facades\Auth;
use Spatie\QueryBuilder\AllowedFilter;
use Spatie\QueryBuilder\QueryBuilder;

class AdController extends Controller
{
    public function index()
    {
        // Get Data
        $admin = Auth::guard('api_admin')->user();
        // Authorize the action
        $this->authorize('viewAny', Ad::class);

        $ads = (new GetAdsAction)->execute($admin)
            ->paginate(request('perPage', 5));

        $total  = $ads->total();


        // Return Response
        return response()->success(
            'this is all ads',
            [
                "ad" => AdResource::collection($ads),
                "total" => $total,
            ]
        );
    }


    public function store(StoreAdRequest $request)
    {
        // Store Ad
        $ad = Ad::create($request->validated());
        $ad->load('restaurant.owner');

        // Add Image to Ad
        $ad
            ->addMediaFromRequest('image')
            ->toMediaCollection('Ad');

        // Return Response
        return response()->success(
            'ad is added success',
            [
                "ad" => new AdResource($ad),
            ]
        );
    }


    public function show(Ad $ad)
    {
        // Return Response
        $admin = Auth::guard('api_admin')->user();
        $ad->load('restaurant.owner');

        // Authorize the action
        $this->authorize('view', [Ad::class, $ad]);

        return response()->success(
            'this is your ad',
            [
                "ad" => new AdResource($ad),
            ]
        );
    }

    public function toggleActive(Ad $ad)
    {
        // Return Response
        $ad->update(['is_active' => !$ad->is_active]);
        return response()->success(
            'ad active is toggled success',
        );
    }

    public function update(UpdateAdRequest $request, Ad $ad)
    {
        // Update Ad
        $ad->update($request->validated());

        $ad->load('restaurant.owner');


        // Edit Image for  Ad if exist
        $request->hasFile('image') &&
            $ad
            ->addMediaFromRequest('image')
            ->toMediaCollection('Ad');



        // Return Response
        return response()->success(
            'ad is updated success',
            [
                "ad" => new AdResource($ad),
            ]
        );
    }

    public function destroy(Ad $ad)
    {
        $admin = Auth::guard('api_admin')->user();
        // Authorize the action
        $this->authorize('delete', [Ad::class, $ad]);

        // Delete Ad
        $ad->delete();

        // Return Response
        return response()->success('ad is deleted success');
    }
}
