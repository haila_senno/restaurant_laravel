<?php

namespace App\Http\Controllers\Api\Admin;

use App\Http\Controllers\Controller;
use App\Models\Contact;
use App\Http\Requests\Admin\Contact\StoreContactRequest;
use App\Http\Requests\Admin\Contact\UpdateContactRequest;
use App\Http\Requests\Admin\Contact\UpdatePriorityRequest;
use App\Http\Resources\Admin\ContactResource;
use Spatie\QueryBuilder\AllowedFilter;
use Spatie\QueryBuilder\QueryBuilder;

class ContactController extends Controller
{
    public function index()
    {
        // Get Data
        $contacts = Contact::orderBy('priority')
            ->paginate(request('perPage'));
        // Return Response
        return response()->success(
            'this is all Contacts',
            [
                "contacts" => ContactResource::collection($contacts),
            ]
        );
    }


    public function store(StoreContactRequest $request)
    {
        // Store Contact
        $contact = Contact::create($request->validated());
        // Return Response
        return response()->success(
            'contact is added success',
            [
                "contact" => new ContactResource($contact),
            ]
        );
    }



    public function update(UpdateContactRequest $request, Contact $contact)
    {
        // Update Contact
        $contact->update($request->validated());


        // Return Response
        return response()->success(
            'contact is updated success',
            [
                "contact" => new ContactResource($contact),
            ]
        );
    }

    public function updatePriorities(UpdatePriorityRequest $request)
    {
        $contacts = $request->validated()['contacts'];
        foreach($contacts as $contact)
        {
            Contact::where('id',$contact['id'])->update(['priority' => $contact['priority']]);
        }
        return response()->success(
            'priorities updated success',
            [
                'contacts' => ContactResource::collection(Contact::orderBy('priority')->get())
            ]
        );
    }

    public function destroy(Contact $contact)
    {
        // Delete Contact
        $contact->delete();

        // Return Response
        return response()->success('contact is deleted success');
    }
}
