<?php

namespace App\Http\Controllers\Api\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\User\StoreUserRequest;
use App\Http\Requests\Admin\User\UpdateUserRequest;
use App\Http\Resources\Admin\UserResource;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Spatie\QueryBuilder\AllowedFilter;
use Spatie\QueryBuilder\QueryBuilder;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = QueryBuilder::for(User::class)
            ->with('media')
            ->allowedFilters('name')
            ->latest('id')
            ->paginate(request('perPage', 5));

        $total = $users->total();
        return response()->success(
            'this is all Users',
            [
                "users" => UserResource::collection($users),
                "total" => $total
            ]
        );
    }

    public function mostUsingCoupons(Request $request)
    {
        $startDate = $request->input('startDate') ?? Carbon::today()->subDays(7);
        $endDate = $request->input('endDate') ?? Carbon::today();

        // $users = QueryBuilder::for(User::query())
        // ->allowedFilters([
        //     AllowedFilter::scope('start_date'),
        //     AllowedFilter::scope('end_date')
        // ])
        //     // ->betweenDates($startDate,$endDate)
        //     ->withCount('coupons')
        //     ->orderBy('coupons_count', 'desc')
        //     ->get();

        $users = User::query()
            ->couponDates($startDate, $endDate)
            ->withCount('coupons')
            ->orderBy('coupons_count', 'desc')
            ->take(10)
            ->paginate(request('perPage', 5));

        $total = $users->total();

        return response()->success(
            'this is all users using coupons',
            [
                'users' => UserResource::collection($users),
                'total' => $total,
            ]
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function store(StoreUserRequest $request)
    {
        $user = User::create($request->validated());

        $user->addMediaFromRequest('image')
            ->toMediaCollection('User');
        $user->load('media');
        return response()->success(
            'user is added success',
            [
                'user' => new UserResource($user),
            ]
        );
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        $user->load('media');
        return response()->success(
            'this is your user',
            [
                'user' => new UserResource($user),
            ]
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateUserRequest $request, User $user)
    {
        $user->update($request->validated());

        $request->hasFile('image') &&
            $user
            ->addMediaFromRequest('image')
            ->toMediaCollection('User');

        $user->load('media');
        return response()->success(
            'user is updated success',
            [
                'user' => new UserResource($user)
            ]
        );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        $user->delete();
        return response()->success(
            'user is deleted success'
        );
    }
}
