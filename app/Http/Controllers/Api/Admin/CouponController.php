<?php

namespace App\Http\Controllers\Api\Admin;

use App\Http\Controllers\Controller;
use App\Http\Resources\Admin\CouponResource;
use App\Models\Coupon;
use App\Http\Requests\Admin\Coupon\StoreCouponRequest;
use App\Http\Requests\Admin\Coupon\UpdateCouponRequest;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Spatie\QueryBuilder\AllowedFilter;
use Spatie\QueryBuilder\QueryBuilder;

class CouponController extends Controller
{
    public function index()
    {

        // Get Data
        $coupons = Coupon::latest()->paginate(request('perPage', 5));
        $total = $coupons->total();
        // Return Response
        return response()->success(
            'this is all Coupons',
            [
                "coupons" => CouponResource::collection($coupons),
                "total" => $total,
            ]
        );
    }

    public function mostUsed(Request $request)
    {
        $startDate = $request->input('startDate') ?? Carbon::today()->subDays(7);
        $endDate = $request->input('endDate') ?? Carbon::today();

        $coupons = Coupon::query()
            ->couponDates($startDate, $endDate)
            ->withCount('users')
            ->orderBy('users_count', 'desc')
            ->take(10)
            ->paginate(request('perPage', 5));
        $total = $coupons->total();

        return response()->success(
            'this is all coupons most used',
            [
                'coupons' => CouponResource::collection($coupons),
                'total' => $total
            ]
        );
    }

    public function store(StoreCouponRequest $request)
    {
        // Store Coupon
        $coupon = Coupon::create($request->validated());

        // Return Response
        return response()->success(
            'coupon is added success',
            [
                "coupon" => new CouponResource($coupon),
            ]
        );
    }


    public function show(Coupon $coupon)
    {
        // Return Response
        return response()->success(
            'this is your coupon',
            [
                "coupon" => new CouponResource($coupon),
            ]
        );
    }

    public function update(UpdateCouponRequest $request, Coupon $coupon)
    {
        // Update Coupon
        $coupon->update($request->validated());

        // Return Response
        return response()->success(
            'coupon is updated success',
            [
                "coupon" => new CouponResource($coupon),
            ]
        );
    }

    public function destroy(Coupon $coupon)
    {
        // Delete Coupon
        $coupon->delete();

        // Return Response
        return response()->success('coupon is deleted success');
    }
}
