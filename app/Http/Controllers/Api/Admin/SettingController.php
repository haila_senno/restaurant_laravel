<?php

namespace App\Http\Controllers\Api\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Setting\StoreSettingRequest;
use App\Http\Requests\Admin\Setting\UpdateSettingRequest;
use App\Http\Resources\Admin\SettingResource;
use App\Models\Setting;
use Illuminate\Support\Facades\Cache;

class SettingController extends Controller
{
    public function index()
    {
        $setting = Setting::first();
        return response()->success(
            'this is all settings',
            [
                'settings' => new SettingResource($setting),
            ]
        );

    }

    public function store(StoreSettingRequest $request)
    {
        $setting = Setting::create($request->validated());
        // Cache::forget('settings');
        return response()->success(
            'setting is created success',
            [
                'setting' => new SettingResource($setting),
            ]
        );
    }

    public function update(UpdateSettingRequest $request, Setting $setting)
    {
        $setting->update($request->validated());
        // Cache::forget('settings');

        return response()->success(
            'setting is updated success',
            [
                'setting' => new SettingResource($setting),
            ]
        );
    }

    public function destroy(Setting $setting)
    {
        $setting->delete();

        return response()->success(
            'setting is deleted success'
        );
    }
}
