<?php

namespace App\Http\Controllers;

use App\Http\Resources\NotificationUserResource;
use App\Models\NotificationUser;
use App\Http\Requests\StoreNotificationUserRequest;
use App\Http\Requests\UpdateNotificationUserRequest;

use Spatie\QueryBuilder\AllowedFilter;
use Spatie\QueryBuilder\QueryBuilder;

class NotificationUserController extends Controller
{
    public function index()
    {
        // Get Data
        $notificationUsers = NotificationUser::latest()->get();

        // OR with filter

        // $notificationUsers = QueryBuilder::for(NotificationUser::class)
        //     ->allowedFilters([
        //         "test_id",
        //         AllowedFilter::exact('test_id'),
        //     ])->get();


        // Return Response
        return response()->success(
            'this is all NotificationUsers',
            [
                "notificationUsers" => NotificationUserResource::collection($notificationUsers),
            ]
        );
    }


    public function store(StoreNotificationUserRequest $request)
    {
        // Store NotificationUser
        $notificationUser = NotificationUser::create($request->validated());


        // Add Image to NotificationUser
        $notificationUser
            ->addMediaFromRequest('image')
            ->toMediaCollection('NotificationUser');

        // Return Response
        return response()->success(
            'notificationUser is added success',
            [
                "notificationUser" => new NotificationUserResource($notificationUser),
            ]
        );
    }


    public function show(NotificationUser $notificationUser)
    {
        // Return Response
        return response()->success(
            'this is your notificationUser',
            [
                "notificationUser" => new NotificationUserResource($notificationUser),
            ]
        );
    }

    public function update(UpdateNotificationUserRequest $request, NotificationUser $notificationUser)
    {
        // Update NotificationUser
         $notificationUser->update($request->validated());


        // Edit Image for  NotificationUser if exist
        $request->hasFile('image') &&
            $notificationUser
                ->addMediaFromRequest('image')
                ->toMediaCollection('NotificationUser');



        // Return Response
        return response()->success(
            'notificationUser is updated success',
            [
                "notificationUser" => new NotificationUserResource($notificationUser),
            ]
        );
    }

    public function destroy(NotificationUser $notificationUser)
    {
        // Delete NotificationUser
        $notificationUser->delete();

        // Return Response
        return response()->success('notificationUser is deleted success');
    }
}
