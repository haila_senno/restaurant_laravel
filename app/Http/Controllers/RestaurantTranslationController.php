<?php

namespace App\Http\Controllers;

use App\Http\Resources\RestaurantTranslationResource;
use App\Models\RestaurantTranslation;
use Illuminate\Http\Request;

use Spatie\QueryBuilder\AllowedFilter;
use Spatie\QueryBuilder\QueryBuilder;

class RestaurantTranslationController extends Controller
{
    public function index()
    {
        // Get Data
        $restaurantTranslations = RestaurantTranslation::latest()->get();

        // OR with filter

        // $restaurantTranslations = QueryBuilder::for(RestaurantTranslation::class)
        //     ->allowedFilters([
        //         "test_id",
        //         AllowedFilter::exact('test_id'),
        //     ])->get();


        // Return Response
        return response()->success(
            'this is all RestaurantTranslations',
            [
                "restaurantTranslations" => RestaurantTranslationResource::collection($restaurantTranslations),
            ]
        );
    }


    public function store(Request $request)
    {
        // Store RestaurantTranslation
        $restaurantTranslation = RestaurantTranslation::create($request->validated());


        // Add Image to RestaurantTranslation
        $restaurantTranslation
            ->addMediaFromRequest('image')
            ->toMediaCollection('RestaurantTranslation');

        // Return Response
        return response()->success(
            'restaurantTranslation is added success',
            [
                "restaurantTranslation" => new RestaurantTranslationResource($restaurantTranslation),
            ]
        );
    }


    public function show(RestaurantTranslation $restaurantTranslation)
    {
        // Return Response
        return response()->success(
            'this is your restaurantTranslation',
            [
                "restaurantTranslation" => new RestaurantTranslationResource($restaurantTranslation),
            ]
        );
    }

    public function update(Request $request, RestaurantTranslation $restaurantTranslation)
    {
        // Update RestaurantTranslation
         $restaurantTranslation->update($request->validated());


        // Edit Image for  RestaurantTranslation if exist
        $request->hasFile('image') &&
            $restaurantTranslation
                ->addMediaFromRequest('image')
                ->toMediaCollection('RestaurantTranslation');



        // Return Response
        return response()->success(
            'restaurantTranslation is updated success',
            [
                "restaurantTranslation" => new RestaurantTranslationResource($restaurantTranslation),
            ]
        );
    }

    public function destroy(RestaurantTranslation $restaurantTranslation)
    {
        // Delete RestaurantTranslation
        $restaurantTranslation->delete();

        // Return Response
        return response()->success('restaurantTranslation is deleted success');
    }
}
