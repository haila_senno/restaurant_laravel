<?php

namespace App\Http\Controllers;

use App\Http\Resources\CategoryRestaurantResource;
use App\Models\CategoryRestaurant;
use App\Http\Requests\StoreCategoryRestaurantRequest;
use App\Http\Requests\UpdateCategoryRestaurantRequest;

use Spatie\QueryBuilder\AllowedFilter;
use Spatie\QueryBuilder\QueryBuilder;

class CategoryRestaurantController extends Controller
{
    public function index()
    {
        // Get Data
        $categoryRestaurants = CategoryRestaurant::latest()->get();

        // OR with filter

        // $categoryRestaurants = QueryBuilder::for(CategoryRestaurant::class)
        //     ->allowedFilters([
        //         "test_id",
        //         AllowedFilter::exact('test_id'),
        //     ])->get();


        // Return Response
        return response()->success(
            'this is all CategoryRestaurants',
            [
                "categoryRestaurants" => CategoryRestaurantResource::collection($categoryRestaurants),
            ]
        );
    }


    public function store(StoreCategoryRestaurantRequest $request)
    {
        // Store CategoryRestaurant
        $categoryRestaurant = CategoryRestaurant::create($request->validated());


        // Add Image to CategoryRestaurant
        $categoryRestaurant
            ->addMediaFromRequest('image')
            ->toMediaCollection('CategoryRestaurant');

        // Return Response
        return response()->success(
            'categoryRestaurant is added success',
            [
                "categoryRestaurant" => new CategoryRestaurantResource($categoryRestaurant),
            ]
        );
    }


    public function show(CategoryRestaurant $categoryRestaurant)
    {
        // Return Response
        return response()->success(
            'this is your categoryRestaurant',
            [
                "categoryRestaurant" => new CategoryRestaurantResource($categoryRestaurant),
            ]
        );
    }

    public function update(UpdateCategoryRestaurantRequest $request, CategoryRestaurant $categoryRestaurant)
    {
        // Update CategoryRestaurant
         $categoryRestaurant->update($request->validated());


        // Edit Image for  CategoryRestaurant if exist
        $request->hasFile('image') &&
            $categoryRestaurant
                ->addMediaFromRequest('image')
                ->toMediaCollection('CategoryRestaurant');



        // Return Response
        return response()->success(
            'categoryRestaurant is updated success',
            [
                "categoryRestaurant" => new CategoryRestaurantResource($categoryRestaurant),
            ]
        );
    }

    public function destroy(CategoryRestaurant $categoryRestaurant)
    {
        // Delete CategoryRestaurant
        $categoryRestaurant->delete();

        // Return Response
        return response()->success('categoryRestaurant is deleted success');
    }
}
