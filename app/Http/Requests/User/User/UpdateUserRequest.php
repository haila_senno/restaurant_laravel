<?php

namespace App\Http\Requests\User\User;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;

class UpdateUserRequest extends FormRequest
{

    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        $user = Auth::guard('api_user')->user();
        return [
            'name' => ['nullable', 'string'],
            'username' => ['nullable', Rule::unique('users')->ignore($user)],
            'password' => ['nullable', 'confirmed'],
            'phone_number' => ['nullable', 'string', Rule::unique('users')->ignore($user)],
            'image' => ['nullable', 'file'],
        ];
    }


    public function validated($key = null, $default = null): array
    {
        $data =  [
            "name" => $this->name,
            "username" => $this->username,
            "password" => $this->password,
            "phone_number" => $this->phone_number,
        ];
        return array_filter($data, fn ($value) => !is_null($value));
    }
}
