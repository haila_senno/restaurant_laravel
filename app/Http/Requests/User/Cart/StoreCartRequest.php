<?php

namespace App\Http\Requests\User\Cart;

use App\Enums\OrderStatusEnum;
use App\Enums\PaymentMethodEnum;
use App\Rules\CheckProductsRule;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rules\Enum;

class StoreCartRequest extends FormRequest
{

    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return [
            'order_products' => ['array', 'min:1'],
            'order_products.*.product_id' => ['required', 'integer', 'exists:products,id', new CheckProductsRule($this->route('restaurant')->id)],
            'order_products.*.count' => ['required', 'integer'],
            'order_products.*.details' => ['nullable', 'string'],

        ];
}


    public function validated($key = null, $default = null): array
    {
        return data_get($this->validator->validated(), $key, $default);

    }
}
