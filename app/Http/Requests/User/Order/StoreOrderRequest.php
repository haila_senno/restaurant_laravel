<?php

namespace App\Http\Requests\User\Order;

use App\Models\CouponUser;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;

class StoreOrderRequest extends FormRequest
{

    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return [
            "table_id" => [
                "required",
                Rule::exists('tables', 'id')
                    ->where(function ($query) {
                        $query->where('restaurant_id', $this->route('restaurant')->id);
                    })
            ],
            "coupon_id" => ["nullable", "exists:coupons,id"],
        ];
    }



    public function validated($key = null, $default = null): array
    {
        $data = [
            'table_id' => $this->table_id,

        ];
        return $data;
    }

    public function couponValidated($key = null, $default = null): array
    {
        $data = [
            'coupon_id' => $this->coupon_id,

        ];
        return array_filter($data, fn ($value) => !is_null($value));
    }
}
