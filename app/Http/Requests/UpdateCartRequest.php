<?php

namespace App\Http\Requests;

use App\Enums\OrderStatusEnum;
use App\Enums\PaymentMethodEnum;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use Illuminate\Validation\Rules\Enum;

class UpdateCartRequest extends FormRequest
{

    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
       $cart =  $this->route('cart');
       $order = $cart->order_id;

        return [
            'order_products' => ['array', 'min:1'],
            'order_products.*.product_id' => ['nullable', 'integer', 'exists:products,id'],
            'order_products.*.count' => ['nullable', 'integer'],
            "user_code" => ["nullable"],
            "order_number" => ["nullable",Rule::unique('orders')->ignore($order)],
            "table_id" => ["nullable", "exists:tables,id"],
            "payment_method" => ["nullable", "integer", new Enum(PaymentMethodEnum::class)],
            "status"=>["nullable","integer",new Enum(OrderStatusEnum::class)],

        ];
    }


    public function validated($key = null, $default = null): array
    {
        return [
            'cart' => [
                "order_products" => $this->order_products,
                "user_code" => $this->user_code,
            ],

            'order' => [
                "order_number" => $this->order_number,
                "table_id" => $this->table_id,
                "payment_method" => $this->payment_method,
                "status" => $this->status,
            ],
        ];
    }

}
