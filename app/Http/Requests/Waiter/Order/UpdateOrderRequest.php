<?php

namespace App\Http\Requests\Waiter\Order;

use App\Enums\OrderStatusEnum;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rules\Enum;

class UpdateOrderRequest extends FormRequest
{

    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        $order = $this->route('order');
        return [
            'status' => [
                'nullable', new Enum(OrderStatusEnum::class), function ($attribute, $value, $fail) use ($order) {
                    if ($order->status == OrderStatusEnum::COMPLETED->value) {
                        $fail('The order is completed you can not change its status');
                    }
                    else if ($order->status == OrderStatusEnum::PREPARING->value &&  $value == OrderStatusEnum::PENDING->value) {
                        $fail('The order status is preparing you can not change it to pending');
                    }
                },
            ],
        ];
    }


    public function validated($key = null, $default = null): array
    {
        return data_get($this->validator->validated(), $key, $default);
    }
}
