<?php

namespace App\Http\Requests\Waiter\Order;

use Illuminate\Foundation\Http\FormRequest;


class ShowOrderRequest extends FormRequest
{

    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return [
            "qr_code" => ["required", "string", "exists:orders,qr_code"]
        ];
    }


    public function validated($key = null, $default = null): array
    {
        return data_get($this->validator->validated(), $key, $default);
    }
}
