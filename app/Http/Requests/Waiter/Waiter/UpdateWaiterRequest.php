<?php

namespace App\Http\Requests\Waiter\Waiter;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;

class UpdateWaiterRequest extends FormRequest
{

    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        $waiter =  Auth::guard('api_waiter')->user();

        return [
            "restaurant_id" => ["nullable", "exists:restaurants,id"],
            "name" => ["nullable", "string"],
            "username" => ["nullable", "string", Rule::unique('waiters')->ignore($waiter)],
            "password" => ["nullable",'confirmed'],
            "phone_number" => ["nullable", "numeric", Rule::unique('waiters')->ignore($waiter)],
            "image" => ["nullable", "file"],
        ];
    }


    public function validated($key = null, $default = null): array
    {
        $data = [
            "restaurant_id" => $this->restaurant_id,
            "name" => $this->name,
            "username" => $this->username,
            "password" => $this->password,
            "phone_number" => $this->phone_number,
        ];

        return array_filter($data,fn($value) => !is_null($value));
    }
}
