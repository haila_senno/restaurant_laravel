<?php

namespace App\Http\Requests\Auth\Admin;

use Illuminate\Foundation\Http\FormRequest;


class RegisterAdminRequest extends FormRequest
{

    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return [
            "name" => ["required"],
            "username" => ["required", "unique:admins,username"],
            "password" => ["required","confirmed"],
            "image"=>["required","file"]
        ];
    }


    public function validated($key = null, $default = null): array
    {
        return [
            "name" => $this->name,
            "username" => $this->username,
            "password" => $this->password,
        ];
    }
}
