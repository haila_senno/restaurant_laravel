<?php

namespace App\Http\Requests\Auth\Waiter;

use Illuminate\Foundation\Http\FormRequest;


class RegisterWaiterRequest extends FormRequest
{

    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return [
            "name" => ["required", "string"],
            "username" => ["required", "unique:waiters,username"],
            "password" => ["required", "confirmed"],
            "phone_number" => ["nullable", "numeric"],
            "gender" => ["nullable", "string"],
            "restaurant_id" => ["required"]
        ];
    }


    public function validated($key = null, $default = null): array
    {
        return data_get($this->validator->validated(), $key, $default);
    }
}
