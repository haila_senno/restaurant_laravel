<?php

namespace App\Http\Requests\Auth\User;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rules\Password;

class RegisterUserRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return [
            'name' => ['required', 'string'],
            'username' => ['required', 'unique:users,username'],
            'phone_number' => ['required', 'unique:users,phone_number'],
            'password' => ['required', 'confirmed'],
            'image' => ['required', 'file']
        ];
    }


    public function validated($key = null, $default = null): array
    {

        return [
            "name" => $this->name,
            "username" => $this->username,
            "password" => $this->password,
            "phone_number" => $this->phone_number,
        ];
    }
}
