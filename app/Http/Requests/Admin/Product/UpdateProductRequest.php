<?php

namespace App\Http\Requests\Admin\Product;

use App\Enums\AdminPermissionEnum;
use App\Enums\AdminRoleEnum;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class UpdateProductRequest extends FormRequest
{

    public function authorize(): bool
    {
        $admin = Auth::guard('api_admin')->user();
        $product = $this->route('product');

        if ($admin->hasRole(AdminRoleEnum::OWNER->value) && $admin->can(AdminPermissionEnum::UPDATE_PRODUCT->value)) {
            $restaurant = $product->categoryRestaurant->restaurant;
            return $restaurant->owner_id === $admin->id;
        }

        return true;
    }

    public function rules(): array
    {
        return [
            "name" => ["nullable", "string"],
            "price" => ["nullable", "numeric"],
            "details" => ["nullable", "string"],
            "is_shown" => ["nullable", "boolean"],
            "image" => ["nullable", "file"],

        ];
    }


    public function validated($key = null, $default = null): array
    {
        $data = [
            "name" => $this->name,
            "price" => $this->price,
            "details" => $this->details,
            "is_shown" => $this->is_shown,
        ];

        return array_filter($data, fn ($value) => !is_null($value));
    }
}
