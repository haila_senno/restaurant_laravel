<?php

namespace App\Http\Requests\Admin\Product;

use Illuminate\Foundation\Http\FormRequest;


class StoreProductRequest extends FormRequest
{

    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return [

            "name" => ["required", "string"],
            "price" => ["required", "numeric"],
            "details" => ["nullable", "string"],
            "is_shown" => ["required", "boolean"],
            "image" => ["required", "file"],
        ];
    }


    public function validated($key = null, $default = null): array
    {
        return [
            "name" => $this->name,
            "price" => $this->price,
            "details" => $this->details,
            "is_shown" => $this->is_shown,
        ];
    }
}
