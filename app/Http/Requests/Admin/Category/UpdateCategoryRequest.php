<?php

namespace App\Http\Requests\Admin\Category;

use App\Enums\AdminPermissionEnum;
use App\Enums\AdminRoleEnum;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class UpdateCategoryRequest extends FormRequest
{

    public function authorize(): bool
    {
        $category = $this->route('category');
        $admin = Auth::guard('api_admin')->user();
        if (($admin->hasRole(AdminRoleEnum::OWNER->value) &&
            $admin->can(AdminPermissionEnum::UPDATE_CATEGORY->value)
        )) {

            $restaurantIds = $category->restaurants->pluck('owner_id')->toArray();
            return in_array($admin->id, $restaurantIds);
        }

        return true;
    }

    public function rules(): array
    {
        return [
            "name" => ["nullable", "string"],
        ];
    }


    public function validated($key = null, $default = null): array
    {
        return data_get($this->validator->validated(), $key, $default);
    }
}
