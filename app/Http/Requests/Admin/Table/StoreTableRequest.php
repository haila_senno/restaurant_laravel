<?php

namespace App\Http\Requests\Admin\Table;

use Illuminate\Foundation\Http\FormRequest;


class StoreTableRequest extends FormRequest
{

    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        $restaurant_id = $this->route('restaurant')->id;

        return [

            "table_number" => ["required", "numeric", "unique:tables,table_number,NULL,NULL,restaurant_id," . $restaurant_id],
        ];
    }

    public function messages()
    {
        return [
            'table_number.unique' => 'this table number has been taken in this restaurant',
        ];
    }


    public function validated($key = null, $default = null): array
    {
        return data_get($this->validator->validated(), $key, $default);
    }
}
