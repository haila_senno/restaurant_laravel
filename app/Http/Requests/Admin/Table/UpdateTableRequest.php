<?php

namespace App\Http\Requests\Admin\Table;

use App\Enums\AdminPermissionEnum;
use App\Enums\AdminRoleEnum;
use App\Models\Restaurant;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;

class UpdateTableRequest extends FormRequest
{

    public function authorize(): bool
    {
        $admin = Auth::guard('api_admin')->user();
        if (($admin->hasRole(AdminRoleEnum::OWNER->value) &&
            $admin->can(AdminPermissionEnum::UPDATE_TABLE->value)
        ))

         {
            $restaurant = $this->filled('restaurant_id') ? Restaurant::findOrFail($this->restaurant_id) : $this->route('table')->restaurant;

            return $restaurant && $restaurant->owner_id === $admin->id;
        }

        return true;
    }

    public function rules(): array
    {
        return [
            "table_number" => ["nullable", "numeric",
            Rule::unique('tables')
                    ->where(function ($query) {
                        $restaurantId = $this->filled('restaurant_id') ? $this->restaurant_id : $this->route('table')->restaurant->id;
                        return $query->where('restaurant_id', $restaurantId);

                    })
                    ->ignore($this->route('table'))
            ],
            "restaurant_id" => ["nullable", "exists:restaurants,id"],
        ];
    }

    public function messages()
    {
        return [
            'table_number.unique' => 'this table number has been taken in this restaurant',
        ];
    }

    public function validated($key = null, $default = null): array
    {
        return data_get($this->validator->validated(), $key, $default);
    }
}
