<?php

namespace App\Http\Requests\Admin\Coupon;

use Illuminate\Foundation\Http\FormRequest;


class UpdateCouponRequest extends FormRequest
{

    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return [
            "description" => ['nullable', 'string'],
            // "start_date" => ['nullable', 'date'],
            // "end_date" => ['nullable', 'date'],
            "discount" => ['nullable', 'numeric', 'regex:/^\d+(\.\d+)?$/'],
            "points" => ['nullable', 'integer'],
            "min_bill" => ['nullable', 'numeric', 'regex:/^\d+(\.\d+)?$/'],
            "is_available" => ['nullable', 'boolean'],
        ];
    }


    public function validated($key = null, $default = null): array
    {
        return data_get($this->validator->validated(), $key, $default);

    }
}
