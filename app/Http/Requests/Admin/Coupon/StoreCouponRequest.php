<?php

namespace App\Http\Requests\Admin\Coupon;

use Illuminate\Foundation\Http\FormRequest;


class StoreCouponRequest extends FormRequest
{

    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return [
            "description" => ['nullable', 'string'],
            // "start_date" => ['required', 'date'],
            // "end_date" => ['required', 'date'],
            "discount" => ['required', 'numeric', 'regex:/^\d+(\.\d+)?$/'],
            "points" => ['required', 'integer'],
            "min_bill" => ['required', 'numeric', 'regex:/^\d+(\.\d+)?$/'],
        ];
    }


    public function validated($key = null, $default = null): array
    {
        return data_get($this->validator->validated(), $key, $default);
    }
}
