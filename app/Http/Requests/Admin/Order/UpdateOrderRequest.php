<?php

namespace App\Http\Requests\Admin\Order;

use App\Models\CouponUser;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;

class UpdateOrderRequest extends FormRequest
{

    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
{
        return [
            "waiter_id" => ["nullable", "numeric","exists:waiters,id"],

        ];
    }



    public function validated($key = null, $default = null): array
    {
        return data_get($this->validator->validated(), $key, $default);

    }


}
