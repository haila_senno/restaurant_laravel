<?php

namespace App\Http\Requests\Admin\User;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class UpdateUserRequest extends FormRequest
{

    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return [
            'name' => ['nullable', 'string'],
            'username' => ['nullable', Rule::unique('users')->ignore($this->route('user'))],
            'phone_number' => ['nullable', Rule::unique('users')->ignore($this->route('user'))],
            'password' => ['nullable', 'confirmed'],
            'image' => ['nullable', 'file']
        ];
    }


    public function validated($key = null, $default = null): array
    {
        $data =  [
            "name" => $this->name,
            "username" => $this->username,
            "password" => $this->password,
            "phone_number" => $this->phone_number,
        ];
        return array_filter($data,fn($value) => !is_null($value));    }

}
