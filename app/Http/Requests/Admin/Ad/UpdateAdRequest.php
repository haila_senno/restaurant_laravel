<?php

namespace App\Http\Requests\Admin\Ad;

use App\Enums\AdminPermissionEnum;
use App\Enums\AdminRoleEnum;
use App\Models\Restaurant;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class UpdateAdRequest extends FormRequest
{

    public function authorize(): bool
    {
        $admin = Auth::guard('api_admin')->user();
        if (($admin->hasRole(AdminRoleEnum::OWNER->value) &&
            $admin->can(AdminPermissionEnum::UPDATE_AD->value) &&
            $this->filled('restaurant_id')
        )) {
            $restaurant = Restaurant::findOrFail($this->restaurant_id);

            return $restaurant && $restaurant->owner_id === $admin->id;
        }

        return true;    }

    public function rules(): array
    {
        return [
            "image" => ["nullable", "file"],
            "restaurant_id" => ["nullable", "exists:restaurants,id"]
        ];
    }


    public function validated($key = null, $default = null): array
    {
        $data =  [
            "restaurant_id" => $this->restaurant_id
        ];

        return array_filter($data, fn($value) => !is_null($value));
    }
}
