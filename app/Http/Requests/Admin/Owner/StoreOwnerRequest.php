<?php

namespace App\Http\Requests\Admin\Owner;

use Illuminate\Foundation\Http\FormRequest;


class StoreOwnerRequest extends FormRequest
{

    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return [
            "name" => ["required"],
            "username" => ["required", "unique:admins,username"],
            "password" => ["required","confirmed"],
            "phone_number" => ["required","numeric","unique:admins,phone_number"],
            "image" => ["required","file"],
        ];
    }


    public function validated($key = null, $default = null): array
    {
         $data = [
            "name" => $this->name,
            "username" => $this->username,
            "password" => $this->password,
            "phone_number" => $this->phone_number,
        ];

        return $data;
    }

}
