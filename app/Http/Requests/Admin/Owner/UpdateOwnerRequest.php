<?php

namespace App\Http\Requests\Admin\Owner;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class UpdateOwnerRequest extends FormRequest
{

    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        $owner = $this->route('owner');
        return [
            "name" => ["nullable"],
            "username" => ["nullable", Rule::unique("admins")->ignore($owner)],
            "password" => ["nullable","confirmed"],
            "phone_number" => ["nullable","numeric",Rule::unique("admins")->ignore($owner)],
            "image" => ["nullable","file"],
        ];
    }


    public function validated($key = null, $default = null): array
    {
        $data = [
            "name" => $this->name,
            "username" => $this->username,
            "password" => $this->password,
            "phone_number" => $this->phone_number,
        ];

        return array_filter($data,fn($value) => !is_null($value));
      }

}
