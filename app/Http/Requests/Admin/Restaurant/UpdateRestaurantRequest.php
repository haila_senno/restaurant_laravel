<?php

namespace App\Http\Requests\Admin\Restaurant;

use App\Rules\CheckOwnerRule;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class UpdateRestaurantRequest extends FormRequest
{

    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return [
            "name" => ["nullable", "string",Rule::unique('restaurants')->ignore($this->route('restaurant'))],
            "address" => ["nullable", "string"],
            "description" => ["nullable", "string"],
            "tax" => ["nullable", "numeric"],
            "owner_id" => ["nullable", new CheckOwnerRule],
            "image" => ["nullable", "file"],
        ];

    }


    public function validated($key = null, $default = null): array
    {
        $data =  [
            "name" => $this->name,
            "address" => $this->address,
            "description" => $this->description,
            "tax" => $this->tax,
            "owner_id" => $this->owner_id,
        ];
        return array_filter($data,fn($value) => !is_null($value));
    }

}
