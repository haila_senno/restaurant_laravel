<?php

namespace App\Http\Requests\Admin\Restaurant;

use App\Rules\CheckOwnerRule;
use Illuminate\Foundation\Http\FormRequest;


class StoreRestaurantRequest extends FormRequest
{

    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {

        return [
            "name" => ["required", "string","unique:restaurants,name"],
            "address" => ["required", "string"],
            "description" => ["nullable", "string"],
            "tax" => ["required", "numeric"],
            "owner_id" => ["required", new CheckOwnerRule],
            "image" => ["required", "file"],
        ];
    }


    public function validated($key = null, $default = null): array
    {

        return [
            "name" => $this->name,
            // "restaurant_code" => $this->restaurant_code,
            "address" => $this->address,
            "description" => $this->description,
            "tax" => $this->tax,
            "owner_id" => $this->owner_id,
        ];
    }
}
