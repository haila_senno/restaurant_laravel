<?php

namespace App\Http\Requests\Admin\Waiter;

use App\Enums\AdminPermissionEnum;
use App\Enums\AdminRoleEnum;
use App\Models\Restaurant;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class StoreWaiterRequest extends FormRequest
{
    public function authorize(): bool
    {
        $admin = Auth::guard('api_admin')->user();
        if (($admin->hasRole(AdminRoleEnum::OWNER->value) &&
            $admin->can(AdminPermissionEnum::CREATE_WAITER->value))) {

            $restaurant = Restaurant::findOrFail($this->restaurant_id);

            return $restaurant && $restaurant->owner_id === $admin->id;
        }

        return true;
    }

    public function rules(): array
    {
        return [
            'restaurant_id' => ['required', 'exists:restaurants,id'],
            'name' => ['required', 'string'],
            'username' => ['required', 'string', 'unique:waiters,username'],
            'password' => ['required', 'confirmed'],
            'phone_number' => ['required', 'numeric', 'unique:waiters,phone_number'],
            'image' => ['required', 'file'],
        ];
    }

    public function validated($key = null, $default = null): array
    {
        return [
            'restaurant_id' => $this->restaurant_id,
            'name' => $this->name,
            'username' => $this->username,
            'password' => $this->password,
            'phone_number' => $this->phone_number,
        ];
    }
}
