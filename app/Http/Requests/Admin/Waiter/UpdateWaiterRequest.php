<?php

namespace App\Http\Requests\Admin\Waiter;

use App\Enums\AdminPermissionEnum;
use App\Enums\AdminRoleEnum;
use App\Models\Restaurant;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;

class UpdateWaiterRequest extends FormRequest
{
    public function authorize(): bool
    {
        $admin = Auth::guard('api_admin')->user();
        if (($admin->hasRole(AdminRoleEnum::OWNER->value) &&
            $admin->can(AdminPermissionEnum::UPDATE_WAITER->value) &&
            $this->filled('restaurant_id')
        )) {
            $restaurant = Restaurant::findOrFail($this->restaurant_id);

            return $restaurant && $restaurant->owner_id === $admin->id;
        }

        return true;
    }

    public function rules(): array
    {
    // $waiter =  $this->route('waiter');

        return [
            'restaurant_id' => ['nullable', 'exists:restaurants,id'],
            'name' => ['nullable', 'string'],
            'username' => ['nullable', 'string', Rule::unique('waiters')->ignore($this->route('waiter'))],
            'password' => ['nullable', 'confirmed'],
            'phone_number' => ['nullable', 'numeric', Rule::unique('waiters')->ignore($this->route('waiter'))],
            'image' => ['nullable', 'file'],
        ];
    }

    public function validated($key = null, $default = null): array
    {
        $data = [
            'restaurant_id' => $this->restaurant_id,
            'name' => $this->name,
            'username' => $this->username,
            'password' => $this->password,
            'phone_number' => $this->phone_number,
        ];

        return array_filter($data, fn ($value) => ! is_null($value));
    }
}
