<?php

namespace App\Http\Requests\Admin\Contact;

use App\Enums\ContactTypeEnum;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use Illuminate\Validation\Rules\Enum;

class UpdateContactRequest extends FormRequest
{

    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        $contact = $this->route('contact');
        return [
            'contact_type' => ["nullable", new Enum(ContactTypeEnum::class)],
            'details' => [
                "nullable", "string",
                function ($attribute, $value, $fail) use ($contact) {
                    if ($this->filled('contact_type')) {
                        if ($this->contact_type == 3 && !is_numeric($value)) {
                            $fail($attribute . ' must be a number when contact type is 3.');
                        }
                    } else if ($contact && $contact->contact_type == 3 && !is_numeric($value)) {
                        $fail($attribute . ' must be a number when contact type is 3.');
                    }
                }
            ],
            'priority' => ["nullable",Rule::unique('contacts')->ignore($this->route('contact'))]
        ];
    }


    public function validated($key = null, $default = null): array
    {
        return data_get($this->validator->validated(), $key, $default);
    }
}
