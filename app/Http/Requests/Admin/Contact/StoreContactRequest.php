<?php

namespace App\Http\Requests\Admin\Contact;

use App\Enums\ContactTypeEnum;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rules\Enum;

class StoreContactRequest extends FormRequest
{

    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return [
            'contact_type' => ["required", new Enum(ContactTypeEnum::class)],
            'details' => [
                "required", "string",
                function ($attribute, $value, $fail) {
                    if ($this->contact_type == 3 && !is_numeric($value))
                        $fail($attribute . ' must be a number when contact type is 3.');
                }

            ],
            'priority' => ["required","integer",'unique:contacts,priority'],
        ];
    }


    public function validated($key = null, $default = null): array
    {
        return data_get($this->validator->validated(), $key, $default);
    }
}
