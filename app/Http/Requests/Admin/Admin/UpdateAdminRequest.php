<?php

namespace App\Http\Requests\Admin\Admin;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;

class UpdateAdminRequest extends FormRequest
{

    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        $admin = Auth::guard('api_admin')->user();
        return [
            "name" => ["nullable", "string"],
            "username" => ["nullable", Rule::unique("admins")->ignore($admin)],
            "password" => ["nullable", "confirmed"],
            "phone_number" => ["nullable", "string", Rule::unique("admins")->ignore($admin)],
            "image" => ["nullable", "file"]

        ];
    }


    public function validated($key = null, $default = null): array
    {
        $data = [
            "name" => $this->name,
            "username" => $this->username,
            "password" => $this->password,
            "phone_number" => $this->phone_number,
        ];

        return array_filter($data, fn ($value) => !is_null($value));
    }
}
