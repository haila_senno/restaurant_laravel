<?php

namespace App\Http\Resources\User;

use Illuminate\Http\Resources\Json\JsonResource;


class UserResource extends JsonResource
{


    public function toArray($request): array
    {
        return [
            'id' =>$this->id,
            'name' => $this->name,
            'username' => $this->username,
            'phone_number' => $this->phone_number,
            'total_points' => $this->total_points,
            'image' => $this->getFirstMediaUrl('User')
        ];
    }
}
