<?php

namespace App\Http\Resources\User;

use App\Http\Resources\User\TableResource;
use Illuminate\Http\Resources\Json\JsonResource;


class RestaurantResource extends JsonResource
{


    public function toArray($request): array
    {
        //return parent::toArray($request);


        return [
            'id' => $this->id,
            'name' => $this->name,
            'description' => $this->description ?? '',
            'address' => $this->address ?? '',
            'tax' => $this->tax ?? '',
            'owner_name' => $this->whenLoaded('owner', function () {
                return $this->owner->name;
            }),
            'image' => $this->whenLoaded('media', function () {
                return $this->getFirstMediaUrl('Restaurant');
            }),
            'tables' => $this->whenLoaded('tables', function () {
                return TableResource::collection($this->tables);
            }),

         ];
    }
}
