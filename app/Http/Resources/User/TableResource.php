<?php

namespace App\Http\Resources\User;

use Illuminate\Http\Resources\Json\JsonResource;


class TableResource extends JsonResource
{


    public function toArray($request): array
    {

        return [
            'id' => $this->id,
            'table_number' => $this->table_number,
            'restaurant_name' => $this->whenLoaded('restaurant', function () {
                return $this->restaurant->name;
            })
        ];
    }
}
