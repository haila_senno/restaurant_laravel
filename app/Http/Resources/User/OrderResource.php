<?php

namespace App\Http\Resources\User;

use App\Http\Resources\Admin\RestaurantResource;
use Illuminate\Http\Resources\Json\JsonResource;


class OrderResource extends JsonResource
{


    public function toArray($request): array
    {


        return [
            'id' => $this->id,
            'created_at' => $this->created_at->format('d/m/Y'),
            'user_name' => $this->whenLoaded('user', function () {
                return $this->user->name;
            }),
            'waiter_name' => $this->whenLoaded('waiter', function () {
                return $this->waiter->name;
            }),
            'restaurant_name' => $this->whenLoaded('waiter', function () {
                return $this->waiter->restaurant->name;
            }),
            'table_number' => $this->whenLoaded('table', function () {
                return $this->table->table_number;
            }),
            'status' => $this->status,
            'price' => $this->price,


            'coupon_discount' => $this->couponUser->coupon->discount ?? '',

            'restaurant_tax' => $this->whenLoaded('waiter', function () {
                return $this->waiter->restaurant->tax;
            }),
            'total_points' => $this->total_points,
            // 'total_period' => $this->total_period,
            'total_price' => $this->total_price,
            // 'total_period' => $this->total_period,

           'products' => $this->whenLoaded('products', function () {
                return CartResource::collection($this->products);
            }),







            // 'created_at'=>$this->created_at->format('d M Y'),
            //y-m-d

        ];
    }
}
