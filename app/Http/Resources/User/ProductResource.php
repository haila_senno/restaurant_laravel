<?php

namespace App\Http\Resources\User;

use Illuminate\Http\Resources\Json\JsonResource;

class ProductResource extends JsonResource
{
    public function toArray($request): array
    {

        return [

            'id' => $this->id,
            'name' => $this->name,
            'price' => $this->price,
            'points' => $this->points,
            'details' => $this->details,
            'restaurant_id' => $this->whenLoaded('categoryRestaurant', function () {
                return $this->categoryRestaurant->restaurant->id;
            }),
            'image' => $this->whenLoaded('media', function () {
                return $this->getFirstMediaUrl('Product');
            }),
            'is_shown' => $this->is_shown

        ];
    }
}
