<?php

namespace App\Http\Resources\User;

use Illuminate\Http\Resources\Json\JsonResource;

class AdResource extends JsonResource
{
    public function toArray($request): array
    {

        return [
            'id' => $this->id,
            'image' => $this->whenLoaded('media', $this->getFirstMediaUrl('Ad')),
            // 'is_active' => $this->is_active,
            'restaurant_name'=>$this->whenLoaded('restaurant',function() {
                return $this->restaurant->name;
            }),
            'restaurant_owner_name'=>$this->whenLoaded('restaurant',function() {
                return $this->restaurant->owner->name;
            })
        ];

    }
}
