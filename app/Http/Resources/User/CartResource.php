<?php

namespace App\Http\Resources\User;

use Illuminate\Http\Resources\Json\JsonResource;

class CartResource extends JsonResource
{
    public function toArray($request): array
    {

        return [
            'id' => $this->id,
            'product_name' => $this->name,
            'product_price' => $this->pivot->price,
            'product_count' => $this->pivot->count,
            'product_details' => $this->pivot->details ?? '',
            'image' => $this->whenLoaded('media', function () {
                return $this->getFirstMediaUrl('Product');
            }),
        ];
    }
}
