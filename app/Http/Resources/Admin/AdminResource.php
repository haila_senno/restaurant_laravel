<?php

namespace App\Http\Resources\Admin;

use Illuminate\Http\Resources\Json\JsonResource;

class AdminResource extends JsonResource
{
    public function toArray($request): array
    {

        return [
            'id' => $this->id,
            'name' => $this->name,
            'username' => $this->username,
            'phone_number' => $this->phone_number,
            'created_at' => $this->created_at->format('d/m/Y'),
            'role' => $this->whenLoaded('roles', function () {
                return $this->roles->pluck('name');
            }),
            'image' => $this->whenLoaded('media', function () {
                return $this->getFirstMediaUrl('Admin');
            }),
        ];
    }
}
