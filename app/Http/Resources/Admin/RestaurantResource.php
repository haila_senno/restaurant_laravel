<?php

namespace App\Http\Resources\Admin;

use Illuminate\Http\Resources\Json\JsonResource;


class RestaurantResource extends JsonResource
{


    public function toArray($request): array
    {
        //return parent::toArray($request);


        return [
            'id' => $this->id,
            'name' => $this->name,
            'address' => $this->when($request->routeIs('restaurant.*'),function() {
                return $this->address;
            }),
            'description' => $this->when($request->routeIs('restaurant.*'),function() {
                return $this->description ?? '';
            }),
            'tax' => $this->when($request->routeIs('restaurant.*'),function() {
                return $this->tax;
            }) ,
            'owner' => $this->whenLoaded('owner', function () {
                return new AdminResource($this->owner);
            }),
            'products_count' => $this->when($request->routeIs('restaurant.*'),function() {
                return $this->products_count ?? 0;
             }),
            'categories_count' => $this->when($request->routeIs('restaurant.*'),function() {
                return $this->categories_count ?? 0;
            }),
            'created_at'=> $this->when($request->routeIs('restaurant.*'),function() {
                return $this->created_at->format('d/m/Y');
             }),
            'image' => $this->when($request->routeIs('restaurant.*'),function() {
                return $this->getFirstMediaUrl('Restaurant');
            }),
        ];
    }
}
