<?php

namespace App\Http\Resources\Admin;

use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;


class NotificationResource extends JsonResource
{


    public function toArray($request): array
    {
        return [
            'id' => $this->id,
            'title' => $this->title,
            'body' => $this->body,
            'is_sent' => $this->is_sent,
            'send_at' =>  Carbon::parse($this->send_at)->format('d-m-y'),
            'read_at' => $this->pivot ? ($this->pivot->read_at !== null ? true : false)  : false,
            'relatable_id'   => $this->relatable_id,
            'relatable_type' => $this->relatable_type,

        ];
    }
}
