<?php

namespace App\Http\Resources\Admin;

use Illuminate\Http\Resources\Json\JsonResource;


class ContactResource extends JsonResource
{


    public function toArray($request): array
    {
        return [
            'id'             => $this->id,
            'contact_type'             => $this->contact_type,
            'details'             => $this->details,
            'priority'             => $this->priority,
        ];
    }
}
