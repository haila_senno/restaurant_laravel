<?php

namespace App\Http\Resources\Admin;

use Illuminate\Http\Resources\Json\JsonResource;

class CouponResource extends JsonResource
{
    public function toArray($request): array
    {

        return [
            'id' => $this->id,
            // 'start_date' => $this->start_date,
            // 'end_date' => $this->end_date,
            'discount' => $this->discount,
            'points' => $this->points,
            'min_bill' => $this->min_bill,
            'is_available' => $this->is_available,
            'description' => $this->description ?? '',
        ];
    }
}
