<?php

namespace App\Http\Resources\Admin;

use Illuminate\Http\Resources\Json\JsonResource;

class SettingResource extends JsonResource
{
    public function toArray($request): array
    {

        return [
            'id' => $this->id,
            'price' => $this->price,
            'points' => $this->points,
        ];
    }
}
