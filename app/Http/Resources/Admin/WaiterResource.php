<?php

namespace App\Http\Resources\Admin;

use Illuminate\Http\Resources\Json\JsonResource;


class WaiterResource extends JsonResource
{


    public function toArray($request): array
    {
        return [

            'id' => $this->id,
            'name' => $this->name,
            'username' => $this->username,
            'phone_number' => $this->phone_number,
            'restaurant_name' => $this->whenLoaded('restaurant', function () {
                return new RestaurantResource($this->restaurant);
            }),
            'orders_count'=>$this->orders_count ?? '',
            'image' => $this->whenLoaded('media',function() {
                return $this->getFirstMediaUrl('Waiter');
            }),
        ];
    }
}
