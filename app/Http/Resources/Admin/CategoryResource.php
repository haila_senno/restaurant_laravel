<?php

namespace App\Http\Resources\Admin;

use Illuminate\Http\Resources\Json\JsonResource;


class CategoryResource extends JsonResource
{


    public function toArray($request): array
    {


        return [
            'id'             => $this->id,
            'name'             => $this->name,
            'products_count'             => $this->products_count ?? 0,

        ];
    }
}
