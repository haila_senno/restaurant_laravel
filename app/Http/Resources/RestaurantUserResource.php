<?php

namespace App\Http\Resources\Waiter;

use Illuminate\Http\Resources\Json\JsonResource;


class RestaurantUserResource extends JsonResource
{

    public function toArray($request): array
    {
        return [
            'id' => $this->id,
            'restaurant_name' => $this->whenLoaded('restaurant', function () {
                return $this->restaurant->name;
            }),
            'user_name' => $this->whenLoaded('user', function () {
                return $this->restaurant->name;
            }),

            'total_points' => $this->total_points,
        ];
    }
}
