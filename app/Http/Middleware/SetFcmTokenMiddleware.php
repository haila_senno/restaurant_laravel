<?php

namespace App\Http\Middleware;

use Closure;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class SetFcmTokenMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {

        $response = $next($request);
        $fcm_token = $request->header('fcm_token');

        $guards = ['api_user', 'waiter', 'admin'];

        try {
            if ($fcm_token !== null) {

                foreach ($guards as $guard)
                    if (
                        $request->is("api/{$guard}/*") &&
                        ($user = Auth::guard($guard)->user()) &&
                        ($user->fcm_token != $fcm_token)
                    ) {

                        $user->update(
                            ["fcm_token" => $fcm_token]
                        );
                    }
            }
        } catch (Exception $e) {
        }
        return $response;
    }
}
