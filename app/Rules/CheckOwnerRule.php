<?php

namespace App\Rules;

use App\Enums\AdminRoleEnum;
use App\Models\Admin;
use Illuminate\Contracts\Validation\Rule;

class CheckOwnerRule implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $owner = Admin::findOrFail($value);
        return $owner->hasRole(AdminRoleEnum::OWNER->value);
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'the user must be an owner';
    }
}
