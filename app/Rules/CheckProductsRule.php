<?php

namespace App\Rules;

use App\Models\Product;
use Illuminate\Contracts\Validation\Rule;

class CheckProductsRule implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    protected $restaurantId;
    protected $message;

    public function __construct($restaurantId)
    {
        $this->restaurantId = $restaurantId;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        // dd($value);
            // dd($productId);
            $productRestaurantId = Product::findOrFail($value)?->categoryRestaurant->restaurant_id;
            if ($productRestaurantId != $this->restaurantId) {
                $this->message =  'product must belong to the specified restaurant.';
                return false;
            }

        return true;

    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
         return $this->message ?? 'The validation error message.';
    }
}
