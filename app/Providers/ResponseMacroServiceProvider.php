<?php

namespace App\Providers;

use Illuminate\Support\Facades\Response;
use Illuminate\Support\ServiceProvider;

class ResponseMacroServiceProvider extends ServiceProvider
{

    public function boot()
    {
        Response::macro('success', function ($message = "", $data = null) {
            return Response::json([
                'success'  => true,
                'message'  => $message,
                'data'     => $data,
            ]);
        });

        Response::macro('error', function ($message, $status = 500) {
            return Response::json([
                'success'  => false,
                'message'  => $message,
                'data'     => null,
            ], $status);
        });
    }
}
