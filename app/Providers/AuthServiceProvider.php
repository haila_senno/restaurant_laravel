<?php

namespace App\Providers;

use App\Enums\AdminRoleEnum;
use App\Models\Ad;
use App\Models\Category;
use App\Models\Restaurant;
use App\Models\Table;
use App\Models\Waiter;
use App\Policies\AdPolicy;
use App\Policies\CategoryPolicy;
use App\Policies\RestaurantPolicy;
use App\Policies\TablePolicy;
use App\Policies\WaiterPolicy;
use Illuminate\Support\Facades\Gate;

use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The model to policy mappings for the application.
     *
     * @var array<class-string, class-string>
     */
    protected $policies = [
        Category::class => CategoryPolicy::class,
        Restaurant::class => RestaurantPolicy::class,
        Waiter::class => WaiterPolicy::class,
        Table::class => TablePolicy::class,
        Ad::class => AdPolicy::class,

    ];

    /**
     * Register any authentication / authorization services.
     */
    public function boot(): void
    {
        Gate::before(function($user,$ability) {
            return $user->hasRole(AdminRoleEnum::ADMIN->value) ? true : null;
        });
    }
}
