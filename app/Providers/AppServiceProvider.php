<?php

namespace App\Providers;

use App\Enums\AdminRoleEnum;
use App\Models\Setting;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Gate;


class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     */
    public function register(): void
    {
        if ($this->app->environment('local')) {
            $this->app->register(\Laravel\Telescope\TelescopeServiceProvider::class);
            $this->app->register(TelescopeServiceProvider::class);
        }
    }

    /**
     * Bootstrap any application services.
     */
    public function boot(): void
    {
        
        Model::preventSilentlyDiscardingAttributes();
        Model::preventLazyLoading(!app()->isProduction());

        // $this->app->singleton('settings', function () {
        //     return Cache::rememberForever('settings', function () {
        //         return Setting::all()->pluck('price', 'points');
        //     });
        // });
    }
}
