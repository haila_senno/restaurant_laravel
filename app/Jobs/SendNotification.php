<?php

namespace App\Jobs;

use App\Http\Resources\Admin\NotificationResource;
use App\Models\Notification;
use Carbon\Carbon;
use Exception;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class SendNotification implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(protected Notification $notification, protected $fcmTokens)
    {
    }

    private function pushNotification(): void
    {
        //if the notification is deleted the job should not failed
        $notification = Notification::find($this->notification);

        if ($notification)
        {
            try {
                $SERVER_API_KEY = env("FIRE_BASE_SERVER_KEY");

                $data = [
                    'registration_ids' => $this->fcmTokens,
                    'data' => NotificationResource::make($this->notification),
                ];
                $dataString = json_encode($data);
                $headers = [
                    'Authorization: key=' . $SERVER_API_KEY,
                    'Content-Type: application/json',
                ];
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
                curl_setopt($ch, CURLOPT_POST, true);
                curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_POSTFIELDS, $dataString);
                curl_exec($ch);

                if (!$this->notification->is_sent)
                    $this->notification->update([
                        'is_sent' => true,
                        'send_at' => Carbon::now(),
                    ]);
            } catch (Exception $e) {
                throw $e;
            }
        }
    }

    /**
     * @throws Exception
     */
    public function handle(): void
    {
        $this->pushNotification();
    }
}
