<?php
namespace App\Enums;
use ArchTech\Enums\{InvokableCases, Names, Options, Values};

enum OrderStatusEnum: int
{
    use Names,Values,Options,InvokableCases;

    case PENDING = 1;
    case PREPARING = 2;
    case COMPLETED = 3;
    case CANCELED = 4;
}

