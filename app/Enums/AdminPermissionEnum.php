<?php

namespace App\Enums;

use ArchTech\Enums\{InvokableCases, Names, Options, Values};

enum AdminPermissionEnum: int
{
    use Names, Values, Options, InvokableCases;

    case CREATE_PRODUCT = 1;
    case UPDATE_PRODUCT = 2;
    case DELETE_PRODUCT = 3;
    case SHOW_PRODUCT = 4;

    case CREATE_CATEGORY = 5;
    case UPDATE_CATEGORY = 6;
    case DELETE_CATEGORY = 7;
    case SHOW_CATEGORY = 8;

    case CREATE_TABLE = 9;
    case UPDATE_TABLE = 10;
    case DELETE_TABLE = 11;
    case SHOW_TABLE = 12;

    case CREATE_WAITER = 13;
    case UPDATE_WAITER = 14;
    case DELETE_WAITER = 15;
    case SHOW_WAITER = 16;

    case SHOW_RESTAURANT = 17;
    case UPDATE_RESTAURANT= 18;

    case CREATE_AD = 19;
    case UPDATE_AD = 20;
    case DELETE_AD = 21;
    case SHOW_AD = 22;
}
