<?php
namespace App\Enums;
use ArchTech\Enums\{InvokableCases, Names, Options, Values};

enum AdminRoleEnum: int
{
    use Names,Values,Options,InvokableCases;

    case ADMIN = 1;
    case OWNER = 2;
}

