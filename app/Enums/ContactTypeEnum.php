<?php

namespace App\Enums;

use ArchTech\Enums\{InvokableCases, Names, Options, Values};

enum ContactTypeEnum: int
{

    use Names, Values, Options, InvokableCases;

    case FACEBOOK = 1;
    case INSTAGRAM = 2;
    case PHONE_NUMBER = 3;
}
