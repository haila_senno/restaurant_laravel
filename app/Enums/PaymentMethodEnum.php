<?php
namespace App\Enums;
use ArchTech\Enums\{InvokableCases, Names, Options, Values};

enum PaymentMethodEnum: int
{
    use Names,Values,Options,InvokableCases;

    case CASH = 1;
    case POINTS = 2;
}

