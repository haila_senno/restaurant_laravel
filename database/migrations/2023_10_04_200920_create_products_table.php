<?php

use App\Models\Category;
use App\Models\CategoryRestaurant;
use App\Models\Restaurant;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{

    public function up(): void
    {
        Schema::create('products', function (Blueprint $table) {
            $table->id();
            $table->string('name')->nullable();
            $table->double('price');
            $table->unsignedInteger('period');
            $table->unsignedBigInteger('points');
            $table->text('details')->nullable();

            $table->unsignedBigInteger('category_restaurant_id');

            $table->foreign('category_restaurant_id')
                ->references('id')
                ->on('category_restaurants')
                ->onDelete('cascade');
            $table->timestamps();
        });
    }

    public function down(): void
    {
        Schema::dropIfExists('products');
    }
};
