<?php

use App\Enums\OrderStatusEnum;
use App\Models\CouponUser;
use App\Models\Table;
use App\Models\User;
use App\Models\Waiter;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{

    public function up(): void
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->id();
            $table->foreignIdFor(User::class)->constrained('users')->cascadeOnDelete();
            $table->foreignIdFor(Waiter::class)->constrained('waiters')->cascadeOnDelete();
            $table->foreignIdFor(Table::class)->constrained('tables')->cascadeOnDelete();
            $table->foreignIdFor(CouponUser::class)->nullable()->constrained('coupon_users')->cascadeOnDelete();
            $table->string('qr_code')->unique();
            // $table->unsignedInteger('total_period');
            $table->unsignedInteger('total_points');
            $table->unsignedTinyInteger('status')->default(OrderStatusEnum::PENDING->value);
            $table->double('total_price');
            $table->double('price');
            $table->timestamps();
        });
    }

    public function down(): void
    {
        Schema::dropIfExists('orders');
    }
};
