<?php

use App\Models\Restaurant;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    public function up(): void
    {
        Schema::create('restaurant_translations', function (Blueprint $table) {
            $table->id();

            $table->string('name');
            $table->text('description')->nullable();
            $table->string('locale')->index();

            $table->foreignIdFor(Restaurant::class)->constrained('restaurants')->cascadeOnDelete();
            $table->unique(['restaurant_id', 'locale']);


            $table->timestamps();
        });
    }

    public function down(): void
    {
        Schema::dropIfExists('restaurant_translations');
    }
};
