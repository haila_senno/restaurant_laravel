<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{

    public function up(): void
    {
        Schema::create('coupons', function (Blueprint $table) {
            $table->id();

            $table->string('description')->nullable();
            $table->double('discount');
            $table->unsignedBigInteger('points');
            $table->double('min_bill');
            $table->boolean('is_available')->default(true);

            ######## Foreign keys  ########

            // $table->foreignIdFor(City::class)->constrained('cities')->cascadeOnDelete();

            $table->timestamps();
        });
    }

    public function down(): void
    {
        Schema::dropIfExists('coupons');
    }
};
