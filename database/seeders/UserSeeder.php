<?php

namespace Database\Seeders;

use App\Models\User;
use App\Services\CodeGenerator;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;


class UserSeeder extends Seeder
{


    public function run(): void
    {
        // $qrCode = CodeGenerator::generateUniqueCode(User::class, 'user_code', 8);

         //don't duplicate the record if it exists
        \App\Models\User::firstOrCreate([
            'name' => "nour",
            'username' => "nour",
            'password' => Hash::make('12345678'),
            'remember_token' => Str::random(10),
            'total_points' => 0,
            'phone_number' => "0991998522",
            // 'user_code'=> $qrCode
        ]);

        // User::factory(5)->create();

    }
}
