<?php

namespace Database\Seeders;

use App\Models\RestaurantUser;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class RestaurantUserSeeder extends Seeder
{


    public function run(): void
    {
        RestaurantUser::factory(30)->create();

    }
}
