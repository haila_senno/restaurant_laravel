<?php

namespace Database\Seeders;

use App\Models\Restaurant;
use App\Models\Waiter;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class WaiterSeeder extends Seeder
{


    public function run(): void
    {
        Waiter::firstOrCreate([
            "name" => "waiter",
             "username" => "waiter",
             "password" => "12345678",
             "phone_number" => "09898654",
             'restaurant_id' => Restaurant::inRandomOrder()->first()->id,
            ]);

        Waiter::factory(30)->create();

    }
}
