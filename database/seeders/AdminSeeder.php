<?php

namespace Database\Seeders;

use App\Enums\AdminRoleEnum;
use App\Models\Admin;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;


class AdminSeeder extends Seeder
{


    public function run(): void
    {
        //don't duplicate the record if it exists
        Admin::firstOrCreate([
            'name' => "admin",
            'username' => "admin",
            'password' => Hash::make('12345678'),
            'remember_token' => Str::random(10),
            'phone_number' => "0955454939",
            ])->assignRole(AdminRoleEnum::ADMIN->value);

        // Admin::factory(30)->create()
        // ->each(fn($admin) => $admin->assignRole(AdminRoleEnum::OWNER->value));

    }
}
