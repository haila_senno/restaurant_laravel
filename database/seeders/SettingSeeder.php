<?php

namespace Database\Seeders;

use App\Models\Setting;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class SettingSeeder extends Seeder
{


    public function run(): void
    {
         Setting::create([
            'price'=>1000,
            'points'=>100
         ]);

         Setting::create([
            'price'=>2000,
            'points'=>200
         ]);
         Setting::create([
            'price'=>3000,
            'points'=>300
         ]);
    }
}
