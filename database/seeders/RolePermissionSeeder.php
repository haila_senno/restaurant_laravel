<?php

namespace Database\Seeders;

use App\Enums\AdminPermissionEnum;
use App\Enums\AdminRoleEnum;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class RolePermissionSeeder extends Seeder
{


    public function run(): void
    {
        app()[\Spatie\Permission\PermissionRegistrar::class]->forgetCachedPermissions();

        foreach (AdminRoleEnum::values() as $role) {
            Role::create(['guard_name' => 'api_admin', 'name' => $role]);
        }
        app()[\Spatie\Permission\PermissionRegistrar::class]->forgetCachedPermissions();

        foreach (AdminPermissionEnum::options() as $permission) {
            Permission::create([
                'name' => $permission,
                'guard_name' => 'api_admin'
            ]);

            Role::where('name', AdminRoleEnum::OWNER->value)->first()->givePermissionTo($permission);
        }
    }
}
