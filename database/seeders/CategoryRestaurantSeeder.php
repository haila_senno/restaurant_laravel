<?php

namespace Database\Seeders;

use App\Models\CategoryRestaurant;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class CategoryRestaurantSeeder extends Seeder
{


    public function run(): void
    {
        CategoryRestaurant::factory(10)->create();
    }
}
