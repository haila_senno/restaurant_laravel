<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        $this->call([
            UserSeeder::class,
            RolePermissionSeeder::class,
            AdminSeeder::class,
            // RestaurantSeeder::class,
            // TableSeeder::class,
            // CategorySeeder::class,
            // CategoryRestaurantSeeder::class,
            // ProductSeeder::class,
            // WaiterSeeder::class,
            // OrderSeeder::class,
            // CartSeeder::class,
            // RestaurantUserSeeder::class,
            // SettingSeeder::class,
            // CouponSeeder::class,
            // CouponUserSeeder::class,
        ]);
    }
}
