<?php

namespace Database\Factories;

use App\Models\Restaurant;
use Illuminate\Database\Eloquent\Factories\Factory;


class WaiterFactory extends Factory
{

    public function definition(): array
    {
        return [
            "name" => fake()->name(),
            "username" => fake()->unique()->userName(),
            'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
            'phone_number' => fake()->phoneNumber(),
     'restaurant_id' => Restaurant::inRandomOrder()->first()->id,
        ];
    }
}
