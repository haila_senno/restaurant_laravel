<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;


class AdminFactory extends Factory
{

    public function definition(): array
    {

        return [
            'name' => fake()->name(),
            'username' => fake()->unique()->userName(),
            'password' => '12345678',
            'phone_number' => fake()->unique()->phoneNumber(),
        ];
    }
}
