<?php

namespace Database\Factories;

use App\Enums\OrderStatusEnum;
use App\Enums\PaymentMethodEnum;
use App\Models\Order;
use App\Models\Table;
use App\Models\User;
use App\Models\Waiter;
use App\Services\CodeGenerator;
use Illuminate\Database\Eloquent\Factories\Factory;


class OrderFactory extends Factory
{

    public function definition(): array
    {
        $qr_code = CodeGenerator::generateUniqueCode(Order::class,"qr_code",8);
        return [
            // "total_period" => fake()->numberBetween(10, 60),
            "status" => fake()->randomElement(OrderStatusEnum::values()),
            "total_price" => fake()->numberBetween(5000, 50000),
            "price" => fake()->numberBetween(5000, 50000),
            "qr_code" => $qr_code,
            'total_points' => fake()->randomNumber(),

            'user_id' => User::inRandomOrder()->first()->id,
            'waiter_id' => Waiter::inRandomOrder()->first()->id,
            'table_id' => Table::inRandomOrder()->first()->id,

        ];
    }
}
