<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;


class CouponFactory extends Factory
{

    public function definition(): array
    {
        return [
            "discount"   => $this->faker->numberBetween(100,200),
            "points"   => $this->faker->numberBetween(100,200),
            "min_bill"   => $this->faker->numberBetween(1000,2000),
        ];
    }
}
