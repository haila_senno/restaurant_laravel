<?php

namespace Database\Factories;

use App\Models\Category;
use App\Models\Restaurant;
use Illuminate\Database\Eloquent\Factories\Factory;


class CategoryRestaurantFactory extends Factory
{

    public function definition(): array
    {
        return [
            "category_id"   => Category::inRandomOrder()->first()->id,
            "restaurant_id"   => Restaurant::inRandomOrder()->first()->id,
        ];
    }
}
