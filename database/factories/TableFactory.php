<?php

namespace Database\Factories;

use App\Models\Restaurant;
use Illuminate\Database\Eloquent\Factories\Factory;

class TableFactory extends Factory
{
    public function definition(): array
    {
        return [

            'table_number' => fake()->unique()->numberBetween(1, 40),
            'restaurant_id' => Restaurant::inRandomOrder()->first()->id,


        ];
    }
}
