<?php

namespace Database\Factories;

use App\Models\Coupon;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;


class CouponUserFactory extends Factory
{

    public function definition(): array
    {
        return [
            "coupon_id"   => Coupon::inRandomOrder()->first()->id,
            "restaurant_id"   => User::inRandomOrder()->first()->id,        ];
    }
}
