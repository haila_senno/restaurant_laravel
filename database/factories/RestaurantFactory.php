<?php

namespace Database\Factories;

use App\Enums\AdminRoleEnum;
use App\Models\Admin;
use Illuminate\Database\Eloquent\Factories\Factory;


class RestaurantFactory extends Factory
{

    public function definition(): array
    {
        $owner_id = Admin::role(AdminRoleEnum::OWNER->value)->inRandomOrder()->first()->id;
        return [
            "name" => fake()->name(),
            "address" => fake()->address(),
            "description" => fake()->text(),
            "owner_id" => $owner_id,
            "tax" => fake()->randomNumber(),
            // "restaurant_code"=>fake()->ean8(),
        ];
    }
}
