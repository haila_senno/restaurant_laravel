<?php

namespace Database\Factories;

use App\Models\Category;
use App\Models\CategoryRestaurant;
use App\Models\Restaurant;
use Illuminate\Database\Eloquent\Factories\Factory;


class ProductFactory extends Factory
{

    public function definition(): array
    {
        return [
            "name" => fake()->name(),
            "details" => fake()->text(),
            "price" => fake()->randomNumber(),
            "points" => fake()->randomNumber(),
            "category_restaurant_id" => CategoryRestaurant::inRandomOrder()->first()->id,
        ];
    }
}
